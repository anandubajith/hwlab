;
; Evaluate the sine series for n number of terms and 
; compare the result with the output of
; FSIN() instruction in NASM.
;

section .text
global main
extern scanf
extern printf
section .data
main:
    call read_float
    fst qword[x]
    fsin
    call print_float 

    ffree st0
    ffree st1
    ffree st2

    ; need to calculate x - x^3/3! + x^5/5! - x^7/7! + ..
    mov ebx, 1
    mov dx, 1
csin:
    ffree st0
    ffree st1
    ffree st2
    ffree st3
    ffree st4
    ffree st5
    ffree st6
    ffree st7
    fld1
    mov ecx, 1
    pl:
        fmul qword[x]
        inc ecx
        cmp ecx, ebx
        jle pl

    ; calc factorial and divide
    mov byte[f], bl
    call fact
    fild dword[f_res]
    fdivr st1
    fld qword[res]
    fadd st1
    fstp qword[res]
      
    ffree st0
    ffree st1
    ffree st2
    ffree st3
    ffree st4
    ffree st5
    ffree st6
    ffree st7

    fld1
    add ebx,2
    mov ecx, 1
    pl2:
        fmul qword[x]
        inc ecx
        cmp ecx, ebx
        jle pl2

    ; calc factorial and divide
    mov byte[f], bl
    call fact
    fild dword[f_res]
    fdivr st1
    fld qword[res]
    fsub st1
    fstp qword[res]
    ; call print_float
    add ebx, 2
    cmp ebx, 18
    jl csin

    fld qword[res]
    call print_float
    
exit:
    mov eax, 1
    mov ebx, 3
    int 80h

fact:
    pusha
    mov eax, 1
    movzx ebx, byte[f]
    l:
        mul ebx
        dec ebx
        cmp ebx, 1
        jg l
    mov dword[f_res], eax
    popa
    ret
read_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    lea eax, [esp]
    push eax
    push format1
    call scanf
    fld qword[ebp - 8]
    mov esp, ebp
    pop ebp
    ret
print_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format2
    call printf
    mov esp, ebp
    pop ebp
    ret
section .bss
    n: resb 2
    float1: resq 1
    x: resq 1
    y: resq 1
    f: resb 1
    f_res: resd 1
section .data
    format1: db "%lf", 0
    format2: db "%lf", 10
    newline: dw 10
    two: dd 2
    error: dq 0.000001
    res: dq 0.0