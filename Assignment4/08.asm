; 
; Remove duplicate words from a sentence.
; 

section .text
msg1: db 10
section .bss
temp: resb 1
arr: resb 2000
i: resb 1
j: resb 1
n: resb 1
str1: resb 20
str2: resb 20
str3: resb 20
k: resb 1
res: resb 1
section .data
global _start:
_start:

mov byte[i],0
mov byte[j],0
mov ebx,arr
read:
mov eax,3
mov ebx,0
mov ecx,temp
mov edx,1
int 80h
cmp byte[temp],10
je endread
cmp byte[temp],32
jne cont
mov eax,0
mov al,byte[j]
mov bl,20
mul bl
movzx cx,byte[i]
add ax,cx
mov cl,0
mov ebx,arr
mov dx,ax
mov eax,0
movzx eax,dx
mov byte[ebx+eax],0
inc byte[j]
mov byte[i],0
jmp read
cont:
mov eax,0
mov bl,byte[j]
mov al,20
mul bl
movzx cx,byte[i]
add ax,cx
mov cl,byte[temp]
mov ebx,arr
mov byte[ebx+eax],cl
inc byte[i]
jmp read

endread:
mov eax,0
mov bl,byte[j]
mov al,20
mul bl
movzx cx,byte[i]
add ax,cx
mov cl,0
mov ebx,arr
mov byte[ebx+eax],cl

mov al,byte[j]
inc al
mov byte[n],al


mov byte[i],0
mov byte[j],0
for1:
mov cl,byte[n]
cmp byte[i],cl
jnl endfor1
mov eax,0
mov bl,20
mov al,byte[i]
mul bl
mov dl,byte[ebx+eax]
cmp dl,1
jne continue
inc byte[i]
jmp for1
continue:
mov byte[k],0
cpy:
mov eax,0
mov bl,20
mov al,byte[i]
mul bl
movzx cx,byte[k]
add ax,cx
mov ebx,arr
mov dl,byte[ebx+eax]
movzx eax,byte[k]
mov ebx,str1
mov byte[ebx+eax],dl
inc byte[k]
cmp dl,0
je endcpy
jmp cpy

endcpy:

mov cl,byte[i]
mov byte[j],cl
inc byte[j]

for2:
mov cl,byte[n]
cmp byte[j],cl
jnl endfor2
mov byte[k],0

cpy2:
mov eax,0
mov bl,20
mov al,byte[j]
mul bl
movzx cx,byte[k]
add ax,cx
mov ebx,arr
mov dl,byte[ebx+eax]
movzx eax,byte[k]
mov ebx,str2
mov byte[ebx+eax],dl
inc byte[k]
cmp dl,0
je endcpy2
jmp cpy2

endcpy2:
call strcomp
cmp byte[res],1
je duplicate
inc byte[j]
jmp for2

duplicate:
mov eax,0
mov bl,20
mov al,byte[j]
mul bl
mov ebx,arr
mov byte[ebx+eax],1
inc byte[j]
jmp for2

endfor2:
inc byte[i]
jmp for1

endfor1:




printfn:

mov byte[j],0
mov byte[i],0

print:
mov cl,byte[n]
cmp byte[j],cl
jnl endprint
mov ebx,arr
mov eax,0
mov al,byte[j]
mov bl,20
mul bl
movzx cx,byte[i]
add ax,cx
mov ebx,arr
mov cl,byte[ebx+eax]
cmp cl,0
je nextword
cmp cl,1
je dupl
mov byte[temp],cl
mov eax,4
mov ebx,1
mov ecx,temp
mov edx,1
int 80h
inc byte[i]
jmp print

dupl:
    inc byte[j]
    mov byte[i],0
    jmp print

nextword:
    mov byte[temp],32
    inc byte[j]
    mov byte[i],0
    mov eax,4
    mov ebx,1
    mov ecx,temp
    mov edx,1
    int 80h
    jmp print

endprint:

exit:
    mov eax,1
    mov ebx,0
    int 80h


strcomp:
    pusha
    cld
    mov esi,str1
    mov edi,str2
    mov byte[res],0
strcomp2:
    mov al,byte[esi]
    cmpsb
    jne good
check:
    cmp al,0
    je dup
    jmp strcomp2

good:
    mov byte[res],0
    popa
    ret

dup:
    mov byte[res],1
    popa
    ret



