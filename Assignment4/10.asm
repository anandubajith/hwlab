;
; Perform lexicographic sorting of n strings.
; 

section .text
global main
extern scanf
extern printf
section .text
main:
    ; read n strings
    call read_n
    movzx eax, word[num]
    mov bx, 100
    mul bx
    mov ecx ,eax

    mov eax, 0
    read_loop:
        call read_string
        ; copy string from buf to array+100*eax
        mov ebx, 0
        copy_loop:
            mov cl, byte[buf+ebx]
            mov byte[str_array+eax+ebx], cl
            inc ebx
            cmp ebx, 100
            jl copy_loop
        add eax, 100
        cmp ax, cx
        jl read_loop

    ; bubble sort them

    sub ecx, 100

    mov eax, 0
    o1:
        mov ebx, 0
        o2:
            ; compare str_array+ebx, and str_array+ebx+100
            mov dword[a1], str_array
            add dword[a1], ebx
            mov dword[a2], str_array
            add dword[a2], ebx
            add dword[a2], 100
            call strcmp
            cmp byte[diff], 1
            ja noswap
                call swap_strings
            noswap:
            add ebx, 100
            cmp ebx, ecx
            jl o2
        add eax, 100
        cmp eax, ecx
        jl o1

    add ecx, 100
    ; print the result
    call print_newline
    mov eax, 0
    print_loop:
        mov ebx, str_array
        add ebx, eax
        mov dword[loc], ebx
        call print_string
        call print_newline
        add eax, 100
        cmp ax, cx
        jl print_loop

exit:
    mov eax, 1
    mov ebx, 3
    int 80h

strcmp:
    pusha
    ; compare a1, and a2 strings
    mov eax, dword[a1]
    mov ebx, dword[a2]
    compare_loop:
        mov cl, byte[eax]
        mov ch, byte[ebx]
        cmp cl, 0
        je diff_
        cmp cl, ch
        jne diff_
            inc eax
            inc ebx
            jmp compare_loop
    diff_:
        cmp ch, cl
        jg skip
        mov byte[diff],2
        jmp c123
        skip:
            mov byte[diff],1
        c123:
    popa
    ret

swap_strings:
    pusha
    mov eax, dword[a1]
    mov ebx, dword[a2]
    mov edx, 0
    r_pl:
        mov cl, byte[eax]
        mov ch, byte[ebx]
        mov byte[eax], ch
        mov byte[ebx], cl
        inc ebx
        inc eax
        inc edx
        cmp edx, 100
        jl r_pl
    popa
    ret
read_n:
    push ebp
    mov ebp, esp
    sub esp , 2
    lea eax , [ebp-2]
    push eax
    push format3
    call scanf
    mov ax, word[ebp-2]
    mov word[num], ax
    mov esp, ebp
    pop ebp
    ret
read_string:
	pusha
	mov ebx, buf
	mov byte[buf_len], 0
read_char:
	pusha
	mov eax, 3
	mov ebx, 0
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	cmp byte[tmp], 10
	je end_read_string
	inc byte[buf_len]
	mov al, byte[tmp]
	mov byte[ebx], al
	inc ebx
	jmp read_char
end_read_string:
	mov byte[ebx], 0
	popa
	ret
print_string:
	pusha
	mov ebx, dword[loc]
print_char:
	mov al, byte[ebx]
	mov byte[tmp], al
	cmp al, 0
	je end_print_string
	pusha
	mov eax, 4
	mov ebx, 1
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	inc ebx
	jmp print_char
end_print_string:
	popa
	ret
print_newline:
    pusha
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    popa
    ret
section .bss
    n: resb 2
    buf: resb 100
    buf_len: resb 1
    num: resw 1
    tmp: resw 1
    a2: resd 1
    a1: resd 1
    diff: resd 1
    temp: resb 100
    loc: resd 1
section .data
    format1: db "%lf", 0
    format2: db "%lf", 10
    format3: db "%d", 0
    str_array: times 10000 db 0
    nl: db 10