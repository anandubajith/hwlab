;
; Count total number of alphabets, digits,
; special characters and words in a string
;

section .text
global _start
_start:
    mov dword[loc],buf 
	call read_string 

    mov ebx, buf
	movzx ecx, byte[buf_len]
	loop:
		; compare the char
        cmp byte[ebx], 'a'
        jl c1
        cmp byte[ebx], 'z'
        jg c1
        inc byte[alphabet_count]
        jmp skip
        c1:
            cmp byte[ebx], 'A'
            jl c2
            cmp byte[ebx], 'Z'
            jg c2
            inc byte[alphabet_count]
            jmp skip
        c2:
            cmp byte[ebx], '0'
            jl c3
            cmp byte[ebx], '9'
            jg c3
            inc byte[num_count]
            jmp skip
        c3:
            cmp byte[ebx], ' '
            jne c4
            inc byte[word_count]
            jmp skip
        c4:
            inc byte[special_count]
		skip:
		inc ebx
		dec ecx
		cmp ecx, 0
		jg loop

        mov eax, 4
        mov ebx, 1
        mov ecx, special
        mov edx, len_special
        int 80h

        movzx ax, byte[special_count]
        mov word[buf], ax
        call print

        mov eax, 4
        mov ebx, 1
        mov ecx, words
        mov edx, len_words
        int 80h

        inc byte[word_count]
        movzx ax, byte[word_count]
        mov word[buf], ax
        call print

        mov eax, 4
        mov ebx, 1
        mov ecx, alphabet
        mov edx, len_alphabet
        int 80h

        movzx ax, byte[alphabet_count]
        mov word[buf], ax
        call print

        mov eax, 4
        mov ebx, 1
        mov ecx, numbers
        mov edx, len_numbers
        int 80h

        movzx ax, byte[num_count]
        mov word[buf], ax
        call print

exit:
	mov eax, 1
	mov ebx, 0
	int 80h
read_string:
	pusha
	mov ebx, dword[loc] 
	mov byte[buf_len], 0
read_char:
	pusha
	mov eax, 3
	mov ebx, 0
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	cmp byte[tmp], 10
	je end_read_string
	inc byte[buf_len]
	mov al, byte[tmp]
	mov byte[ebx], al
	inc ebx
	jmp read_char
end_read_string:
	mov byte[ebx], 0
	popa
	ret
print_string:
	pusha
	mov ebx, dword[loc]
print_char:
	mov al, byte[ebx]
	mov byte[tmp], al
	cmp al, 0
	je end_print_string
	pusha
	mov eax, 4
	mov ebx, 1
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	inc ebx
	jmp print_char
end_print_string:
	popa
	ret
print:
    mov byte[count], 0
    pusha
    cmp word[buf], 0
    je p0
extract_digits:
    cmp word[buf], 0
    je print_digits
    inc byte[count]
    mov dx, 0
    mov ax, word[buf]
    mov bx, 10
    div bx
    push dx
    mov word[buf], ax
    jmp extract_digits
print_digits:
    cmp byte[count], 0
    je end_print
    dec byte[count]
    pop dx
    mov byte[temp], dl
    add byte[temp], 30h
    mov eax, 4
    mov ebx, 1
    mov ecx, temp
    mov edx, 1
    int 80h
    jmp print_digits
end_print:
    mov eax,4
    mov ebx,1
    mov ecx, separator
    mov edx, 1
    int 80h
    popa
    ret
p0:
    mov eax, 4
    mov ebx, 1
    mov ecx, zero
    mov edx, 1
    int 80h
    jmp end_print


section .bss
    buf: resb 100
	buf_len: resb 1
	result: resb 100
	tmp: resb 1
	loc: resd 1
    char: resb 2
    count: resb 1
    temp: resb 1
section .data
    alphabet_count: db 0
    num_count: db 0
    special_count: db 0
    word_count: db 0
    special: db 'Special Character: '
    len_special: equ $-special
    alphabet: db 'Alphabets: '
    len_alphabet: equ $-alphabet
    numbers: db 'Numbers: '
    len_numbers: equ $-numbers
    words: db 'Words: '
    len_words: equ $-words
    zero: db '0'
    separator: db 10