;
; Remove a given character from a string.
;

section .text
global _start
_start:

	mov eax, 4
	mov ebx, 1
	mov ecx, input_char
	mov edx, len_input_char
	int 80h

    mov eax, 3
    mov ebx, 0
    mov ecx, char
    mov edx, 2
    int 80h

	mov eax, 4
	mov ebx ,1
	mov ecx, input_string
	mov edx, len_input_string
	int 80h

	mov dword[loc],buf 
	call read_string

	mov eax, result
	mov ebx, buf
	movzx ecx, byte[buf_len]
    mov dh, byte[char]
	loop:
		cmp byte[ebx], dh
		je skip
		mov dl, byte[ebx]
		mov byte[eax], dl
		inc eax
		skip:
		inc ebx
		dec ecx
		cmp ecx, 0
		jg loop

	mov eax, 4
	mov ebx, 1
	mov ecx, result_msg
	mov edx, len_result_msg
	int 80h

	mov dword[loc], result	
	call print_string

	mov eax, 4
	mov ebx, 1
	mov ecx, separator
	mov edx, 1
	int 80h
exit:
	mov eax, 1
	mov ebx, 0
	int 80h
read_string:
	pusha
	mov ebx, dword[loc] 
	mov byte[buf_len], 0
read_char:
	pusha
	mov eax, 3
	mov ebx, 0
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	cmp byte[tmp], 10
	je end_read_string
	inc byte[buf_len]
	mov al, byte[tmp]
	mov byte[ebx], al
	inc ebx
	jmp read_char
end_read_string:
	mov byte[ebx], 0
	popa
	ret
print_string:
	pusha
	mov ebx, dword[loc]
print_char:
	mov al, byte[ebx]
	mov byte[tmp], al
	cmp al, 0
	je end_print_string
	pusha
	mov eax, 4
	mov ebx, 1
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	inc ebx
	jmp print_char
end_print_string:
	popa
	ret

section .bss
	buf: resb 100
	buf_len: resb 1
	result: resb 100
	tmp: resb 1
	loc: resd 1
    char: resb 2
section .data
	input_char: db 'Enter character: '
	len_input_char: equ $-input_char
	input_string: db 'Enter string: '
	len_input_string: equ $-input_string
	result_msg: db 'Result: '
	len_result_msg: equ $-result_msg
	separator: db 10
