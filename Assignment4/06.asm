;
; Find the mean, median, standard deviation and variance of n numbers
;

   
section .text
global main
extern scanf
extern printf
section .data
main:

    call read_n

    fldz
    movzx ebx, word[num]
    loop:
        call read_float
        fst qword[array+8*ebx]
        fadd st1
        fst qword[sum]
        dec ebx
        cmp ebx, 0
        jg loop

     ffree st0
    ffree st1
    ffree st2
    ffree st3
    ffree st4
    ffree st5
    ffree st6
    ffree st7

    ; sort the numbers  

    movzx ebx, word[num]
    fldz
    o1:
        movzx ecx, word[num]
        o2:
            fld qword[array+8*ecx]
            fld qword[array+8*(ecx-1)]
            fcomi st1
            ja noswap
                fstp qword[array+8*(ecx)]
                fstp qword[array+8*(ecx-1)]
            noswap:
            ffree st0
            ffree st1
            dec ecx
            cmp ecx, 1
            jg o2
        dec ebx
        cmp ebx, 0
        jg o1

    ; movzx ebx, word[num]
    ; print:
    ;     fld qword[array+8*ebx]
    ;     ; call print_float
    ;     dec ebx
    ;     cmp ebx, 0
    ;     jg print

    movzx ebx, word[num]
    find_s:
        fld qword[array+8*ebx]
        fmul st0
        fadd qword[s_sum]
        fst qword[s_sum]
        dec ebx
        cmp ebx, 0
        jg find_s
        
    fstp qword[s_sum]

    fld qword[sum]
    fidiv word[num]
    call print_float
    fstp qword[average]


    ; median
    xor edx, edx
    movzx eax, word[num]
    mov ebx, 2
    div ebx
    fld qword[array+8*(eax+1)]
    call print_float

    fld qword[average]
    fmul qword[average]
    fst qword[x_2]

    fld qword[s_sum]
    fidiv word[num]
    ; fld qword[x_2]
    fsub st1
    call print_float
    fsqrt
    call print_float


 
exit:
    mov eax, 1
    mov ebx, 3
    int 80h
read_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    lea eax, [esp]
    push eax
    push format1
    call scanf
    fld qword[ebp - 8]
    mov esp, ebp
    pop ebp
    ret
read_n:
    push ebp
    mov ebp, esp
    sub esp , 2
    lea eax , [ebp-2]
    push eax
    push format3
    call scanf
    mov ax, word[ebp-2]
    mov word[num], ax
    mov esp, ebp
    pop ebp
    ret
print_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format2
    call printf
    mov esp, ebp
    pop ebp
    ret
section .bss
    n: resb 2
    float1: resq 1
    x: resq 1
    y: resq 1
    array: resq 100
    sum: resq 1
    x_2: resq 1
    x2: resq 1
    num: resw 1
    average: resq 1
section .data
    format1: db "%lf", 0
    format2: db "%lf", 10
    format3: db "%d",0
    newline: dw 10
    two: dd 2
    s_sum: dq 0.0