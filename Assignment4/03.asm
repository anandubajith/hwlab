;
; Find the square root of a number and compare the 
; result with the output of FSQRT() instruction in NASM.
;

section .text
global main
extern scanf
extern printf
section .text
main:
    call read_float
    fst qword[x]
    fsqrt
    call print_float 

    ffree st0
    ffree st1
    ffree st2

    mov ebx, 100
    fld1
    fst qword[y]
csqrt:
    fld qword[x]
    fdiv qword[y]
    fadd qword[y]
    fidiv dword[two]
    fstp qword[y]
    dec ebx
    cmp ebx, 0
    jg csqrt

    found:
       fld qword[y]
       call print_float


    fld qword[x]
    fsqrt
    fsub qword[y]
    call print_float
    
exit:
    mov eax, 1
    mov ebx, 3
    int 80h
read_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    lea eax, [esp]
    push eax
    push format1
    call scanf
    fld qword[ebp - 8]
    mov esp, ebp
    pop ebp
    ret
print_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format2
    call printf
    mov esp, ebp
    pop ebp
    ret
section .bss
    n: resb 2
    float1: resq 1
    x: resq 1
    y: resq 1
section .data
    format1: db "%lf", 0
    format2: db "%lf", 10
    newline: dw 10
    two: dd 2
    error: dq 0.000001