;
; Find the average of n floating point numbers.
;
section .text
global main
extern scanf
extern printf
section .data
main:
    
    mov word[n], 4


    movzx eax, word[n]
    fldz ; initial 0
    loop:
        ; read a floating point number, 
        ; keep summing
        pusha
        call read_float
        fstp qword[float1]
        fadd qword[float1]
        popa
        dec eax
        cmp eax, 0
        jg loop

    fidiv word[n] ; divide by n
    ; print result
    call print_float
exit:
    mov eax, 1
    mov ebx, 3
    int 80h
read_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    lea eax, [esp]
    push eax
    push format1
    call scanf
    fld qword[ebp - 8]
    mov esp, ebp
    pop ebp
    ret
print_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format2
    call printf
    mov esp, ebp
    pop ebp
    ret
section .bss
    n: resb 2
    float1: resq 1
section .data
    mes1: db "Enter Radius : "
    len1: equ $-mes1
    mes4: db "Area is : "
    len4: equ $-mes4
    format1: db "%lf", 0
    format2: db "%lf", 10
    newline: dw 10