;
; To perform Insertion Sort in an n sized array.
; TODO: working, but the elemets are shifted
%define SIZE 100
section .text
global _start
_start:
    ; read n
    mov eax, 4
    mov ebx, 1
    mov ecx, input_n
    mov edx, len_input_n
    int 80h

    call read
    mov ax, word[buf]
    mov word[n], ax

    ; read array elements
    mov eax, 4
    mov ebx, 1
    mov ecx, input_elem
    mov edx, len_input_elem
    int 80h
    mov eax, 0
    movzx ebx, word[n]
loop:
    call read
    mov dx,word[buf]
    mov byte[ar+eax], dl
    inc eax
    cmp eax, ebx
    jb loop

    mov eax, 1
    movzx ebx, word[n]
outer:
    cmp eax, ebx
    jg q
    mov cl, byte[ar+eax] 
    mov edx, eax
    inner:
        dec edx
        cmp edx, 0
        jl c
        mov ch, byte[ar+edx]
        cmp ch, cl
        jl c
        mov byte[ar+edx+1], ch
        jmp inner
    c:
        mov byte[ar+edx+1], cl
    inc eax
    jmp outer

q:
    mov eax, 1
    movzx ebx, word[n]
pl:
    movzx dx,byte[ar+eax]
    mov word[buf], dx
    call print
    inc eax
    cmp eax, ebx
    jna pl

exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    mov eax, 1
    mov ebx, 2
    int 80h

read:
    pusha
    mov word[buf], 0
read_loop:
    mov eax, 3
    mov ebx, 0
    mov ecx, temp
    mov edx, 1
    int 80h
    cmp byte[temp],030h
    jl end_read
    cmp byte[temp], 039h
    jg end_read
    mov ax, word[buf]
    mov bx, 10
    mul bx
    mov bl, byte[temp]
    sub bl, 30h
    mov bh, 0
    add ax, bx
    mov word[buf], ax
    jmp read_loop
end_read:
    popa
    ret

print:
    mov byte[count], 0
    pusha
    cmp word[buf], 0
    je p0
extract_digits:
    cmp word[buf], 0
    je print_digits
    inc byte[count]
    mov dx, 0
    mov ax, word[buf]
    mov bx, 10
    div bx
    push dx
    mov word[buf], ax
    jmp extract_digits
print_digits:
    cmp byte[count], 0
    je end_print
    dec byte[count]
    pop dx
    mov byte[temp], dl
    add byte[temp], 30h
    mov eax, 4
    mov ebx, 1
    mov ecx, temp
    mov edx, 1
    int 80h
    jmp print_digits
end_print:
    mov eax,4
    mov ebx,1
    mov ecx, separator
    mov edx, 1
    int 80h
    popa
    ret
p0:
    mov eax, 4
    mov ebx, 1
    mov ecx, zero
    mov edx, 1
    int 80h
    jmp end_print


section .bss
    buf: resw 1
    temp: resb 1
    count: resb 1

    n: resw 1
    i: resb 1
    j: resb 1
    ar: resb 200
    key: resw 1 
section .data
    nl:db 0Ah
    separator: db ' '
    zero: db '0'
    input_n: db 'n : '
    len_input_n: equ $-input_n
    input_elem: db 'Enter the elements : '
    len_input_elem: equ $-input_elem
    input_k: db 'k : '
    len_input_k: equ $-input_k
    found_k: db 'Element found'
    len_found_k: equ $-found_k
    not_found_k: db 'Element not found'
    len_not_found_k: equ $-not_found_k