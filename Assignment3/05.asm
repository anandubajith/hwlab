;
; To print all prime numbers in an n sized array
; 

%define SIZE 100
section .text
global _start
_start:
    ; read n
    mov eax, 4
    mov ebx, 1
    mov ecx, input_n
    mov edx, len_input_n
    int 80h

    call read
    mov ax, word[buf]
    mov word[n], ax

    ; read array elements
    mov eax, 4
    mov ebx, 1
    mov ecx, input_elem
    mov edx, len_input_elem
    int 80h
    mov eax, 0
    movzx ebx, word[n]
loop:
    call read
    mov dx,word[buf]
    mov word[ar+eax], dx
    inc eax
    cmp eax, ebx
    jb loop
    
    mov eax, 0
    movzx ebx, word[n]
pl:
    movzx edx,byte[ar+eax]
    mov byte[num], dl
    call check_prime
    ; check if dx is prime, put result in cl
    cmp cl, 0
    je c
    mov word[buf], dx
    call print
c:
    inc eax
    cmp eax, ebx
    jb pl

exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    mov eax, 1
    mov ebx, 2
    int 80h
check_prime:
    pusha
    movzx ax, byte[num]
    cmp ax, 2
    je prime
    mov bl, 2
    div bl
    mov dl, al ; dl, has num/2
    mov cl, 2
loop2:
    movzx ax, byte[num]
    div cl
    cmp ah, 0 ; checking if remainder is zero
    je not_prime
    inc cl
    cmp cl, dl
    jb loop2
    jmp prime
not_prime:
    popa
    mov cl, 0
    ret
prime:
    popa
    mov cl, 1
    ret
read:
    pusha
    mov word[buf], 0
read_loop:
    mov eax, 3
    mov ebx, 0
    mov ecx, temp
    mov edx, 1
    int 80h
    cmp byte[temp],030h
    jl end_read
    cmp byte[temp], 039h
    jg end_read
    mov ax, word[buf]
    mov bx, 10
    mul bx
    mov bl, byte[temp]
    sub bl, 30h
    mov bh, 0
    add ax, bx
    mov word[buf], ax
    jmp read_loop
end_read:
    popa
    ret

print:
    mov byte[count], 0
    pusha
extract_digits:
    cmp word[buf], 0
    je print_digits
    inc byte[count]
    mov dx, 0
    mov ax, word[buf]
    mov bx, 10
    div bx
    push dx
    mov word[buf], ax
    jmp extract_digits
print_digits:
    cmp byte[count], 0
    je end_print
    dec byte[count]
    pop dx
    mov byte[temp], dl
    add byte[temp], 30h
    mov eax, 4
    mov ebx, 1
    mov ecx, temp
    mov edx, 1
    int 80h
    jmp print_digits
end_print:
    mov eax,4
    mov ebx,1
    mov ecx, separator
    mov edx, 1
    int 80h
    popa
    ret
found:
    mov eax,4
    mov ebx,1
    mov ecx, found_k
    mov edx, len_found_k
    int 80h
    jmp exit
not_found:
    mov eax,4
    mov ebx,1
    mov ecx, not_found_k
    mov edx, len_not_found_k
    int 80h
    jmp exit

section .bss
    buf: resw 1
    temp: resb 1
    count: resb 1

    num: resb 1
    n: resw 1
    i: resb 1
    j: resb 1
    ar: times SIZE resw 1
section .data
    separator: db ' '
    nl:db 0Ah
    input_n: db 'n : '
    len_input_n: equ $-input_n
    input_elem: db 'Enter the elements : '
    len_input_elem: equ $-input_elem
    input_k: db 'k : '
    len_input_k: equ $-input_k
    found_k: db 'Element found'
    len_found_k: equ $-found_k
    not_found_k: db 'Element not found'
    len_not_found_k: equ $-not_found_k