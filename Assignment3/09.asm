;
; To perform matrix multiplication. 
; Note: m -> row; n -> col

%define SIZE 100
section .text
global _start
_start:
    ; read m
    mov eax, 4
    mov ebx, 1
    mov ecx, input_m_rc
    mov edx, len_input_m_rc
    int 80h

    call read
    mov ax, word[buf]
    mov word[m1], ax

    call read
    mov ax, word[buf]
    mov word[n1], ax

   
    push word[n1]
    pop word[n]
    push word[m1]
    pop word[m]
    ; read matrix elements
    mov eax, 4
    mov ebx, 1
    mov ecx, input_m1
    mov edx, len_input_m1
    int 80h

    mov ebx, matrix1
    call read_matrix


    ; read n   
    mov eax, 4
    mov ebx, 1
    mov ecx, input_n_rc
    mov edx, len_input_n_rc
    int 80h

    call read
    mov ax, word[buf]
    mov word[m2], ax


    call read
    mov ax, word[buf]
    mov word[n2], ax

  

    push word[n1]
    pop word[n]
    push word[m1]
    pop word[m]

    mov eax, 4
    mov ebx, 1
    mov ecx, input_m2
    mov edx, len_input_m2
    int 80h
    mov ebx, matrix2
    call read_matrix

    mov ax, word[n1]
    cmp ax, word[m2]
    jne invalid
    ; do multiplication and put result here
    
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_res
    mov edx, len_msg_res
    int 80h

mov word[i], 0
multiplication:
    mov word[j], 0
        outer:
            mov word[k],0
            mov word[res], 0
                inner:
                    movzx eax, word[i]
                    movzx edx, word[n1]
                    mul edx
                    movzx edx, word[k]
                    add eax, edx
                    push word[matrix1+ 2*eax]
                    pop word[t1]

                    movzx eax,word[k]
                    movzx edx,word[n2]
                    mul edx
                    add ax,word[j]
                    movzx edx,word[matrix2+2*eax]
                    mov word[t2],dx

                    movzx eax,word[t1]
                    mul edx
                    add ax,word[res]
                    mov word[res],ax

                    inc word[k]
                    movzx eax,word[k]
                    cmp ax,[n1]
                    jb inner

                    push word[res]
                    pop word[buf]
                    call print
                    call print_space

                inc word[j]
                mov ax,word[j]
                cmp ax,word[n2]
                jb outer
                call print_new_line
                inc word[i]
            mov ax,word[i]
            cmp ax,word[m1]
        jb multiplication

    jmp exit

invalid: 
    mov eax, 4
    mov ebx, 1
    mov ecx, invalid_size
    mov edx, len_invalid_size
    int 80h
    call exit

exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    mov eax, 1
    mov ebx, 2
    int 80h

read:
    pusha
    mov word[buf], 0
read_loop:
    mov eax, 3
    mov ebx, 0
    mov ecx, temp
    mov edx, 1
    int 80h
    cmp byte[temp],030h
    jl end_read
    cmp byte[temp], 039h
    jg end_read
    mov ax, word[buf]
    mov bx, 10
    mul bx
    mov bl, byte[temp]
    sub bl, 30h
    mov bh, 0
    add ax, bx
    mov word[buf], ax
    jmp read_loop
end_read:
    popa
    ret

print:
    mov byte[count], 0
    pusha
    cmp word[buf], 0
    je p0
extract_digits:
    cmp word[buf], 0
    je print_digits
    inc byte[count]
    mov dx, 0
    mov ax, word[buf]
    mov bx, 10
    div bx
    push dx
    mov word[buf], ax
    jmp extract_digits
print_digits:
    cmp byte[count], 0
    je end_print
    dec byte[count]
    pop dx
    mov byte[temp], dl
    add byte[temp], 30h
    mov eax, 4
    mov ebx, 1
    mov ecx, temp
    mov edx, 1
    int 80h
    jmp print_digits
end_print:
    popa
    ret
p0:
    mov eax, 4
    mov ebx, 1
    mov ecx, zero
    mov edx, 1
    int 80h
    jmp end_print

read_matrix:
    mov eax, 0
    mov word[i], 0
i_loop:
    mov word[j], 0
j_loop:
    call read
    mov dx , word[buf]
    mov word[ebx + 2 * eax], dx
    inc eax
    inc word[j]
    mov cx, word[j]
    cmp cx, word[n]
    jb j_loop
    inc word[i]
    mov cx, word[i]
    cmp cx, word[m]
    jb i_loop
    ret

print_matrix:
    mov eax, 0
    mov word[i], 0
i_loop2:
    mov word[j], 0
j_loop2:
    ;eax will contain the array index and each element is 2 bytes(1 word) long
    mov dx,word[ebx + 2 * eax]
    mov word[buf],dx
    call print
    pusha
    mov eax, 4
    mov ebx, 1
    mov ecx, tab
    mov edx, 1
    int 80h
    popa
    inc eax
    inc word[j]
    mov cx, word[j]
    cmp cx, word[n]
    jb j_loop2
    pusha
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    popa
    inc word[i]
    mov cx, word[i]
    cmp cx, word[m]
    jb i_loop2
    ret


print_space:
    pusha
    mov eax, 4
    mov ebx, 1
    mov ecx, tab
    mov edx,1
    int 80h
    popa
    ret

print_new_line:
    pusha
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx,1
    int 80h
    popa
    ret
section .bss
    buf: resw 1
    temp: resb 1
    t1: resw 1
    t2: resw 1
    count: resb 1
    m: resw 1
    n: resw 1

    m1: resw 1
    n1: resw 1
    m2: resw 1
    n2: resw 1

    matrix1: resw SIZE
    matrix2: resw SIZE

    i: resw 1
    j: resw 1
    k: resw 1
    res: resw 1

section .data

    matrix3: times SIZE db 0

    nl:db 0Ah
    tab: db 09h
    zero: db '0'
    input_m_rc: db 'Enter rows and columns of matrix1: '
    len_input_m_rc: equ $-input_m_rc
    input_n_rc: db 'Enter rows and columns of matrix2: '
    len_input_n_rc: equ $-input_n_rc
    input_m1: db 'Enter the elements of matrix1 : '
    len_input_m1: equ $-input_m1
    input_m2: db 'Enter the elements of matrix2 : '
    len_input_m2: equ $-input_m2
    invalid_size: db 'Number of columns of matrix1 != Number of columns of matrix2'
    len_invalid_size: equ $-invalid_size
    msg_res: db 'matrix1 x matrix2 = ', 0Ah
    len_msg_res: equ $-msg_res
