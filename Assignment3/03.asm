;
; Sort the elements in an array according to its frequency
; Input  : 2 5 3 2 4 3 4 1 6 4
; Output : 4 4 4 3 3 2 2 6 5 1
;

section .data
global _start
_start:

    call read
    push word[buf]
    pop word[n]

    mov ebx, ar
    mov eax, 0
rl:
    cmp ax, word[n]
    je m
    call read
    push word[buf]
    pop word[ebx+ 2*eax]
    movzx ecx, word[buf]
    inc byte[freq+ecx]
    inc eax
    jmp rl

    ; find the largest in the freq array
    ; print the index freq times
    ; repeat
m:
    mov byte[max_count], 0
    mov byte[max_val], 0
    mov eax, 99
    sl:
    cmp eax, 0
    jl d
    movzx edx, byte[freq +eax]
    cmp dl,byte[max_count]
    jl q
    mov byte[max_count], dl
    mov byte[max_val], al
q:
    dec eax
    jmp sl

d:

    cmp byte[max_val], 0
    je exit

    movzx edx, byte[max_val]
    mov byte[freq+edx], 0

   
    movzx ebx, byte[max_count]
    pl:
        movzx dx, byte[max_val]
        mov word[buf], dx
        call print 
        dec ebx
        cmp ebx, 0
        jg pl

    jmp m


    mov eax, 0
; pl:
;     cmp ax, word[n]
;     je end
;     movzx edx, word[ar+ 2*eax]
;     mov word[buf], dx
;     call print
;     inc eax
;     jmp pl

end:
exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    mov eax, 1
    mov ebx, 2
    int 80h

read:
    pusha
    mov word[buf], 0
read_loop:
    mov eax, 3
    mov ebx, 0
    mov ecx, temp
    mov edx, 1
    int 80h
    cmp byte[temp],030h
    jl end_read
    cmp byte[temp], 039h
    jg end_read
    mov ax, word[buf]
    mov bx, 10
    mul bx
    mov bl, byte[temp]
    sub bl, 30h
    mov bh, 0
    add ax, bx
    mov word[buf], ax
    jmp read_loop
end_read:
    popa
    ret


print:
    mov byte[count], 0
    pusha
    cmp word[buf], 0
    je p0
extract_digits:
    cmp word[buf], 0
    je print_digits
    inc byte[count]
    mov dx, 0
    mov ax, word[buf]
    mov bx, 10
    div bx
    push dx
    mov word[buf], ax
    jmp extract_digits
print_digits:
    cmp byte[count], 0
    je end_print
    dec byte[count]
    pop dx
    mov byte[temp], dl
    add byte[temp], 30h
    mov eax, 4
    mov ebx, 1
    mov ecx, temp
    mov edx, 1
    int 80h
    jmp print_digits
end_print:
    mov eax,4
    mov ebx,1
    mov ecx, separator
    mov edx, 1
    int 80h
    popa
    ret
p0:
    mov eax, 4
    mov ebx, 1
    mov ecx, zero
    mov edx, 1
    int 80h
    jmp end_print
section .bss
    ar: resb 200
    temp: resw 1
    buf: resw 1
    count: resb 1
    max_count: resw 1
    max_val: resw 1
    n: resw 1
section .data
    separator: db ' '
    nl: db 10
    zero db '0'
    freq: times 100 db 0


