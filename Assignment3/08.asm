; 
; To find the transpose of an mXn matrix.
;

%define SIZE 100
section .text
global _start
_start:
    ; read m
    mov eax, 4
    mov ebx, 1
    mov ecx, input_m
    mov edx, len_input_m
    int 80h

    call read
    mov ax, word[buf]
    mov word[m], ax

    ; read n
    mov eax, 4
    mov ebx, 1
    mov ecx, input_n
    mov edx, len_input_n
    int 80h

    call read
    mov ax, word[buf]
    mov word[n], ax

    ; read matrix elements
    mov eax, 4
    mov ebx, 1
    mov ecx, input_m1
    mov edx, len_input_m1
    int 80h


    mov ax, word[m]
    mov bx, word[n]
    mul bx
    mov word[m_n], ax

    mov ebx, matrix1
    call read_matrix
    

    ; print the transpose
    ; ALG:
    ; for (int i = 0; i < m; i++) {
    ;     for (int j = 0; j < m*n; j+=m)
    ;         printf("%d ", b[i+j]);
    ;     printf("\n");   
    ; }
    
    mov eax, 0
    mov word[i], 0
i1_loop:
    pusha
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    popa
    mov word[j], 0
    movzx eax, word[i]
j1_loop:
    mov dx, word[ebx + 2 * eax]
    mov word[buf], dx
    call print
    pusha
    mov eax, 4
    mov ebx, 1
    mov ecx, tab
    mov edx, 1
    int 80h
    popa
    xor ecx, ecx
    mov cx, word[m]
    add eax,ecx
    add word[j], cx
    cmp ax, word[m_n]
    jb j1_loop
    inc word[i]
    xor ecx, ecx
    mov cx, word[i]
    cmp cx, word[m]
    jb i1_loop

exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    mov eax, 1
    mov ebx, 2
    int 80h

read:
    pusha
    mov word[buf], 0
read_loop:
    mov eax, 3
    mov ebx, 0
    mov ecx, temp
    mov edx, 1
    int 80h
    cmp byte[temp],030h
    jl end_read
    cmp byte[temp], 039h
    jg end_read
    mov ax, word[buf]
    mov bx, 10
    mul bx
    mov bl, byte[temp]
    sub bl, 30h
    mov bh, 0
    add ax, bx
    mov word[buf], ax
    jmp read_loop
end_read:
    popa
    ret

print:
    mov byte[count], 0
    pusha
extract_digits:
    cmp word[buf], 0
    je print_digits
    inc byte[count]
    mov dx, 0
    mov ax, word[buf]
    mov bx, 10
    div bx
    push dx
    mov word[buf], ax
    jmp extract_digits
print_digits:
    cmp byte[count], 0
    je end_print
    dec byte[count]
    pop dx
    mov byte[temp], dl
    add byte[temp], 30h
    mov eax, 4
    mov ebx, 1
    mov ecx, temp
    mov edx, 1
    int 80h
    jmp print_digits
end_print:
    popa
    ret

read_matrix:
    mov eax, 0
    mov word[i], 0
i_loop:
    mov word[j], 0
j_loop:
    call read
    mov dx , word[buf]
    mov word[ebx + 2 * eax], dx
    inc eax
    inc word[j]
    mov cx, word[j]
    cmp cx, word[n]
    jb j_loop
    inc word[i]
    mov cx, word[i]
    cmp cx, word[m]
    jb i_loop
    ret

section .bss
    buf: resw 1
    temp: resb 1
    count: resb 1

    m: resw 1
    n: resw 1
    m_n: resw 1
    ar: times SIZE*SIZE resw 1
    mid: resb 1
    num: resb 1

    matrix1: resw 200
i: resw 1
j: resw 1
section .data
    nl:db 0Ah
    input_m: db 'm : '
    len_input_m: equ $-input_m
    input_n: db 'n : '
    len_input_n: equ $-input_n
    input_m1: db 'Enter the elements of first matrix : '
    len_input_m1: equ $-input_m1
    input_m2: db 'Enter the elements of second matrix : '
    len_input_m2: equ $-input_m2
    msg_res: db 'The sum is : ', 0Ah
    len_msg_res: equ $-msg_res
    tab: db 09h
