;
; To perform binary search in an n sized array
; 
%define SIZE 100
section .text
global _start
_start:
    ; read n
    mov eax, 4
    mov ebx, 1
    mov ecx, input_n
    mov edx, len_input_n
    int 80h

    call read
    mov ax, word[buf]
    mov word[n], ax

    ; read array elements
    mov eax, 4
    mov ebx, 1
    mov ecx, input_elem
    mov edx, len_input_elem
    int 80h
    mov eax, 0
    movzx ebx, word[n]
loop:
    call read
    mov dx,word[buf]
    mov word[ar+2*eax], dx
    inc eax
    cmp eax, ebx
    jb loop
    
    ; read k
    mov eax, 4
    mov ebx, 1
    mov ecx, input_k
    mov edx, len_input_k
    int 80h

    call read
    mov ax, word[buf]
    mov word[k], ax

    mov ecx, 0
    movzx edx, word[n]
search:
    cmp ecx, edx
    jg not_found
    mov word[mid], 0
    add word[mid], cx
    add word[mid], dx
    xor eax, eax
    mov ax, word[mid]
    mov bl, 2
    div bl
    mov ah, 0
    mov bx,word[ar+2*eax]
    cmp bx, word[k]
    je found
    jg go_left
go_right:
    inc eax
    mov ecx, eax
    jmp search
go_left:
    dec eax
    mov edx, eax
    jmp search

exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    mov eax, 1
    mov ebx, 2
    int 80h

read:
    pusha
    mov word[buf], 0
read_loop:
    mov eax, 3
    mov ebx, 0
    mov ecx, temp
    mov edx, 1
    int 80h
    cmp byte[temp],030h
    jl end_read
    cmp byte[temp], 039h
    jg end_read
    mov ax, word[buf]
    mov bx, 10
    mul bx
    mov bl, byte[temp]
    sub bl, 30h
    mov bh, 0
    add ax, bx
    mov word[buf], ax
    jmp read_loop
end_read:
    popa
    ret

print:
    mov byte[count], 0
    pusha
extract_digits:
    cmp word[buf], 0
    je print_digits
    inc byte[count]
    mov dx, 0
    mov ax, word[buf]
    mov bx, 10
    div bx
    push dx
    mov word[buf], ax
    jmp extract_digits
print_digits:
    cmp byte[count], 0
    je end_print
    dec byte[count]
    pop dx
    mov byte[temp], dl
    add byte[temp], 30h
    mov eax, 4
    mov ebx, 1
    mov ecx, temp
    mov edx, 1
    int 80h
    jmp print_digits
end_print:
    mov eax,4
    mov ebx,1
    mov ecx, nl
    mov edx, 1
    int 80h
    popa
    ret
found:
    mov eax,4
    mov ebx,1
    mov ecx, found_k
    mov edx, len_found_k
    int 80h
    jmp exit
not_found:
    mov eax,4
    mov ebx,1
    mov ecx, not_found_k
    mov edx, len_not_found_k
    int 80h
    jmp exit

section .bss
    buf: resw 1
    temp: resb 1
    count: resb 1

    n: resw 1
    k: resw 1
    ar: times SIZE resw 1
    mid: resb 1
section .data
    nl:db 0Ah
    input_n: db 'n : '
    len_input_n: equ $-input_n
    input_elem: db 'Enter the elements : '
    len_input_elem: equ $-input_elem
    input_k: db 'k : '
    len_input_k: equ $-input_k
    found_k: db 'Element found'
    len_found_k: equ $-found_k
    not_found_k: db 'Element not found'
    len_not_found_k: equ $-not_found_k