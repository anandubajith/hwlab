;
;Compute f(x) = x 3 +x 2 -5x+9 for floating point x values.
;

section .text
global main:
extern scanf
extern printf
main:
    mov eax, 4
    mov ebx, 1
    mov ecx, mes1
    mov edx, len1
    int 80h
    call read_float
    fstp qword[float1]

    fld qword[float1]
    fmul qword[float1]
    fmul qword[float1]

    fld qword[float1]
    fmul qword[float1]
    fadd ST1
    
    mov dword[tmp], 5
    fild dword[tmp]
    fmul qword[float1]
    fchs

    fadd ST1

    mov dword[tmp], 9
    fiadd dword[tmp] 

    mov eax,4
    mov ebx, 1
    mov ecx, mes2
    mov edx, len2
    int 80h 
    call print_float
    ffree ST0
    ffree ST1
    ffree ST2

exit:
    mov eax, 1
    mov ebx, 0
    int 80h

read_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    lea eax, [esp]
    push eax
    push format1
    call scanf
    fld qword[ebp - 8]
    mov esp, ebp
    pop ebp
    ret
print_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format2
    call printf
    mov esp, ebp
    pop ebp
    ret
print_newline:
    pusha
    mov eax, 4
    mov ebx, 1
    mov ecx, newline
    mov edx, 1
    int 80h
    popa
    ret
section .data
    mes1: db "x = "
    len1: equ $-mes1
    mes2: db "x^3 + x^2 - 5x + 9 = "
    len2: equ $-mes2
    format1: db "%lf", 0
    format2: db "%lf", 10,0
    newline: dw 10
section .bss
    float1: resq 1
    tmp: resq 1
