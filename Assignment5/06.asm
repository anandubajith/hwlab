;
; Sort an array of n floating point numbers.
;

section .text
global main
extern scanf
extern printf
section .data
main:

    mov eax, 4
    mov ebx, 1
    mov ecx, enter_n
    mov edx, len_enter_n
    int 80h
    call read_n

    mov eax, 4
    mov ebx, 1
    mov ecx, enter_v
    mov edx, len_enter_v
    int 80h
 
    fldz
    movzx ebx, word[num]
    loop:
        call read_float
        fstp qword[array+8*ebx]
        dec ebx
        cmp ebx, 0
        jg loop

    call reset_floats
    ; sort the numbers  

    movzx ebx, word[num]
    fldz
    o1:
        movzx ecx, word[num]
        o2:
            fld qword[array+8*ecx]
            fld qword[array+8*(ecx-1)]
            fcomi st1
            ja noswap
                fstp qword[array+8*(ecx)]
                fstp qword[array+8*(ecx-1)]
            noswap:
            call reset_floats
            dec ecx
            cmp ecx, 1
            jg o2
        dec ebx
        cmp ebx, 0
        jg o1

    mov eax, 4
    mov ebx, 1
    mov ecx, sorted
    mov edx, len_sorted 
    int 80h
    
    movzx ebx, word[num]
     print:
         fld qword[array+8*ebx]
         call print_float
         ffree st0
         dec ebx
         cmp ebx, 0
         jg print
exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, newline
    mov edx, 1
    int 80h
    mov eax, 1
    mov ebx, 0
    int 80h
read_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    lea eax, [esp]
    push eax
    push format1
    call scanf
    fld qword[ebp - 8]
    mov esp, ebp
    pop ebp
    ret
read_n:
    push ebp
    mov ebp, esp
    sub esp , 2
    lea eax , [ebp-2]
    push eax
    push format3
    call scanf
    mov ax, word[ebp-2]
    mov word[num], ax
    mov esp, ebp
    pop ebp
    ret
print_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format2
    call printf
    mov esp, ebp
    pop ebp
    ret
reset_floats:
    ffree st0
    ffree st1
    ffree st2
    ffree st3
    ffree st4
    ffree st5
    ffree st6
    ffree st7
    ret 
 
section .bss
    n: resb 2
    float1: resq 1
    x: resq 1
    y: resq 1
    array: resq 100
    sum: resq 1
    x_2: resq 1
    x2: resq 1
    num: resw 1
    average: resq 1
section .data
    sorted: db "The sorted numbers are ", 10
    len_sorted: equ $-sorted
    enter_v: db "Enter the numbers ", 10
    len_enter_v: equ $-enter_v
    enter_n: db "n = "
    len_enter_n: equ $-enter_n
    format1: db "%lf", 0
    format2: db "%lf", 10, 0
    format3: db "%d",0
    newline: dw 10
    two: dd 2
