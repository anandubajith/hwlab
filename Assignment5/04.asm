;
; Program to find sum of cos(x) series:
;       COS X = 1 – (x ^ 2 / 2 !) + (x ^ 4 / 4 !) – (x ^ 6 / 6 ! )+ ..... n terms
; Calculate Cos(X) by processor instruction and compare the result with the above one.
;

section .text
global main
extern scanf
extern printf
main: 
    mov eax, 4
    mov ebx, 1
    mov ecx, enter_x
    mov edx, len_enter_x
    int 80h
   call read_float
   fstp qword[x]
   mov ebx, 4
   fld1
   fstp qword[res]
   cos_p1:
        fld1
        mov ecx, 0
        pl:
            fmul qword[x]
            inc ecx
            cmp ecx, ebx
            jl pl
        mov ecx, 1
        fld1
        fl:
            mov dword[float1], ecx
            fimul dword[float1] 
            inc ecx
            cmp ecx, ebx
            jle fl
        fdivr st1
        fadd qword[res]
        fstp qword[res]
        call reset_floats
        add ebx, 4
        cmp ebx, 50
        jl cos_p1

  mov ebx, 2
  cos_p2:
        fld1
        mov ecx, 0
        pl_2:
            fmul qword[x]
            inc ecx
            cmp ecx, ebx
            jl pl_2
        mov ecx, 1
        fld1
        fl_2:
            mov dword[float1], ecx
            fimul dword[float1] 
            inc ecx
            cmp ecx, ebx
            jle fl_2
        fdivr st1
        fchs
        fadd qword[res]
        fstp qword[res]
        call reset_floats
        add ebx, 4
        cmp ebx, 50
        jl cos_p2


    fld qword[res]
    call print_float
    fld qword[x]
    fcos
    call print_float2
    
    fsub st1
    call print_float3


exit:
    mov eax, 1
    mov ebx, 0
    int 80h
reset_floats:
    ffree st0
    ffree st1
    ffree st2
    ffree st3
    ffree st4
    ffree st5
    ffree st6
    ffree st7
    ret
read_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    lea eax, [esp]
    push eax
    push format1
    call scanf
    fld qword[ebp - 8]
    mov esp, ebp
    pop ebp
    ret
print_float2:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format3
    call printf
    mov esp, ebp
    pop ebp
    ret
print_float3:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format4
    call printf
    mov esp, ebp
    pop ebp
    ret
print_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format2
    call printf
    mov esp, ebp
    pop ebp
    ret
section .bss
    n: resb 2
    float1: resq 1
    x: resq 1
    y: resq 1
section .data
    enter_x: db "x = ", 0
    len_enter_x: equ $-enter_x
    format1: db "%lf", 0
    format2: db "cos(x) by formula = %lf", 10, 0
    format3: db "cos(x) by processor instruction = %lf", 10, 0
    format4: db "difference = %lf", 10, 0
    newline: dw 10
    res: dq 0.0
