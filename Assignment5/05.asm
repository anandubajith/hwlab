;
; Program to find the quadrant in which the coordinate lie.
;

section .text
global main:
extern scanf
extern printf
main:
    mov eax, 4
    mov ebx, 1
    mov ecx, mes1
    mov edx, len1
    int 80h
    call read_float
    fstp qword[x]
    mov eax, 4
    mov ebx, 1
    mov ecx, mes2
    mov edx, len2
    int 80h
    call read_float
    fstp qword[y]

    ; decide the quadrant

    call reset_floats

    fldz
    fld qword[x]
    fcomi st1
    jne x_not_zero
   
        call reset_floats
        fldz
        fld qword[y]
        fcomi st1
        je print_origin
        
        jmp print_yaxis
        
x_not_zero:

    call reset_floats
    fldz
    fld qword[y]
    fcomi st1
    je print_xaxis   
  
    call reset_floats
    fldz
    fld qword[x]
    fcomi st1
    jb x_lt_0
    
    fldz 
    fld qword[y]
    fcomi st1
    ja print_q1
    jb print_q4

x_lt_0:
    fldz 
    fld qword[y]
    fcomi st1
    ja print_q2
    jb print_q3


    mov eax,4
    mov ebx, 1
    
    print_origin:
        mov eax,4
        mov ebx, 1
        mov ecx, origin
        mov edx, len_origin
        int 80h
        jmp exit
    print_xaxis:
        mov eax,4
        mov ebx, 1
       mov ecx, xaxis
        mov edx, len_xaxis
        int 80h
        jmp exit
    print_yaxis:
        mov eax,4
        mov ebx, 1
        mov ecx, yaxis
        mov edx, len_yaxis
        int 80h
        jmp exit
    print_q1:
        mov eax,4
        mov ebx, 1
        mov ecx, first_q 
        mov edx, len_first_q 
        int 80h 
        jmp exit
    print_q2:
         mov eax,4
        mov ebx, 1
        mov ecx, second_q 
        mov edx, len_second_q 
        int 80h 
        jmp exit
    print_q3:
        mov eax,4
        mov ebx, 1
        mov ecx, third_q 
        mov edx, len_third_q 
        int 80h 
        jmp exit
    print_q4:
        mov eax,4
        mov ebx, 1
        mov ecx, fourth_q 
        mov edx, len_fourth_q 
        int 80h 
        jmp exit
exit:
    ffree ST0
    ffree ST1
    ffree ST2
    mov eax, 1
    mov ebx, 0
    int 80h
reset_floats:
    ffree ST0
    ffree ST1
    ffree ST2
    ffree ST3
    ffree ST4
    ffree ST5
    ffree ST6
    ffree ST7
    ret

read_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    lea eax, [esp]
    push eax
    push format1
    call scanf
    fld qword[ebp - 8]
    mov esp, ebp
    pop ebp
    ret
print_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format2
    call printf
    mov esp, ebp
    pop ebp
    ret
print_newline:
    pusha
    mov eax, 4
    mov ebx, 1
    mov ecx, newline
    mov edx, 1
    int 80h
    popa
    ret
section .data
    mes1: db "x = "
    len1: equ $-mes1
    mes2: db "y = "
    len2: equ $-mes2
    origin: db "Origin", 10
    len_origin: equ $-origin
    xaxis: db "X-Axis", 10
    len_xaxis: equ $-xaxis
    yaxis: db "Y-Axis", 10
    len_yaxis: equ $-yaxis
    first_q: db "First quadrant",10
    len_first_q: equ $-first_q
    second_q: db "Second quadrant",10
    len_second_q: equ $-second_q
    third_q: db "Third quadrant",10
    len_third_q: equ $-third_q
    fourth_q: db "Fourth quadrant",10
    len_fourth_q: equ $-fourth_q
    format1: db "%lf", 0
    format2: db "%lf", 10, 0
    newline: dw 10
section .bss
    x: resq 1
    y: resq 1
