;
; Program to find roots of quadratic equation.
;

section .text
global main:
extern scanf
extern printf
main:
    mov eax, 4
    mov ebx, 1
    mov ecx, mes1
    mov edx, len1
    int 80h
    call read_float
    fstp qword[a]
    mov eax, 4
    mov ebx, 1
    mov ecx, mes2
    mov edx, len2
    int 80h
    call read_float
    fstp qword[b]
    mov eax, 4
    mov ebx, 1
    mov ecx, mes3
    mov edx, len3
    int 80h
    call read_float
    fstp qword[c]

    fld qword[b]
    fmul qword[b]
    mov dword[tmp], -4
    fild dword[tmp]
    fmul qword[a]
    fmul qword[c]
    fadd ST1
    ffree ST1
    fst qword[det]
; check if det is negative

    fld qword[det]
    fldz
    fcomi st1 
    jna non_negative_roots

negative_roots:
    call reset_floats
    fld qword[det]
    fchs
    fsqrt
    fstp qword[det]

    call reset_floats
    fldz
    fadd qword[a]
    fadd qword[a]
    
    fld qword[det]
    fdiv st1
    fstp qword[det]
    fld qword[b]
    fdiv st1
    fstp qword[b]
    call reset_floats

    fld qword[det]
    fld qword[b]
    fchs
    call print_complex1
    fld qword[det]
    fld qword[b]
    fchs
    call print_complex2
    jmp exit

non_negative_roots:
    call reset_floats
    fld qword[det]
    fsqrt
    fstp qword[det]

    call reset_floats
    
    fldz
    fadd qword[a]
    fadd qword[a]
    fld qword[b]
    fchs
    fadd qword[det]
    fdiv st1
    mov eax, 4
    mov ebx, 1
    mov ecx, msg4
    mov edx, len4
    int 80h 
    call print_float
    call reset_floats
    
    fldz
    fadd qword[a]
    fadd qword[a]
    fld qword[b]
    fchs
    fsub qword[det]
    fdiv st1
    mov eax,4
    mov ebx, 1
    mov ecx, msg5
    mov edx, len5
    int 80h 
    call print_float
    

exit:
    mov eax, 1
    mov ebx, 0
    int 80h

read_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    lea eax, [esp]
    push eax
    push format1
    call scanf
    fld qword[ebp - 8]
    mov esp, ebp
    pop ebp
    ret
print_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format2
    call printf
    mov esp, ebp
    pop ebp
    ret
print_complex1:
    push ebp
    mov ebp, esp
    sub esp, 16
    fstp qword[ebp - 16]
    fstp qword[ebp - 8]
    push format3
    call printf
    mov esp, ebp
    pop ebp
    ret
print_complex2:
    push ebp
    mov ebp, esp
    sub esp, 16
    fstp qword[ebp - 16]
    fstp qword[ebp - 8]
    push format4
    call printf
    mov esp, ebp
    pop ebp
    ret
print_newline:
    pusha
    mov eax, 4
    mov ebx, 1
    mov ecx, newline
    mov edx, 1
    int 80h
    popa
    ret
reset_floats:
    ffree ST0
    ffree ST1
    ffree ST2
    ffree ST3
    ffree ST4
    ffree ST5
    ffree ST6
    ffree ST7
    ret
section .data
    mes1: db "a = "
    len1: equ $-mes1
    mes2: db "b = "
    len2: equ $-mes2
    mes3: db "c = "
    len3: equ $-mes3
    msg4: db "x1 = "
    len4: equ $-msg4
    msg5: db "x2 = "
    len5: equ $-msg5
    format1: db "%lf", 0
    format2: db "%lf", 10, 0
    format3: db "%lf+i%lf", 10, 0
    format4: db "%lf-i%lf", 10, 0
    newline: dw 10
section .bss
    a: resq 1
    b: resq 1
    c: resq 1
    tmp: resq 1
    det: resq 1
