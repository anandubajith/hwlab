;
; Compute the Taylor series for the exponential function e x at a = 0 is
;

section .text
global main
extern scanf
extern printf
main: 
   mov eax, 4
   mov ebx, 1
   mov ecx, msg
   mov edx, len_msg
   int 80h
   call read_float
   fstp qword[x]
   mov ebx, 1
   fld1
   fstp qword[res]
   exp:
        fld1
        mov ecx, 0
        pl:
            fmul qword[x]
            inc ecx
            cmp ecx, ebx
            jl pl
        mov ecx, 1
        fld1
        fl:
            mov dword[float1], ecx
            fimul dword[float1] 
            inc ecx
            cmp ecx, ebx
            jle fl
        fdivr st1
        fadd qword[res]
        fstp qword[res]
        call reset_floats
        inc ebx
        cmp ebx, 100
        jl exp

   fld qword[res]

   call print_float

exit:
    mov eax, 1
    mov ebx, 0
    int 80h
reset_floats:
    ffree st0
    ffree st1
    ffree st2
    ffree st3
    ffree st4
    ffree st5
    ffree st6
    ffree st7
    ret
read_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    lea eax, [esp]
    push eax
    push format1
    call scanf
    fld qword[ebp - 8]
    mov esp, ebp
    pop ebp
    ret
print_float:
    push ebp
    mov ebp, esp
    sub esp, 8
    fst qword[ebp - 8]
    push format2
    call printf
    mov esp, ebp
    pop ebp
    ret
section .bss
    n: resb 2
    float1: resq 1
    x: resq 1
    y: resq 1
section .data
    msg: db "x = "
    len_msg: equ $-msg
    format1: db "%lf", 0
    format2: db "e^x = %lf", 10, 0
    newline: dw 10
    res: dq 0.0
