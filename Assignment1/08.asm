;
; find factors of two digit number
;
section .text
global _start:
_start:
    mov eax, 4
    mov ebx, 1
    mov ecx, input
    mov edx, len_input
    int 80h
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[num], al
    mov dl, byte[num] ; dl, has num/2
    mov cl, 1
loop:
    mov byte[factor], cl
    movzx ax, byte[num]
    div cl
    cmp ah, 0 ; checking if remainder is zero
    jne continue 
    call print_factor ; if remainder is zero print
continue:
    inc cl
    cmp cl, dl
    jna loop
exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, newline
    mov edx, 1
    int 80h
    mov eax, 1
    mov ebx, 0
    int 80h
print_factor:
    ; storing the registers in stack
    pusha
    ; print the number
    movzx ax, byte[factor]
    mov bl, 10
    div bl
    add ah, 30h
    add al, 30h
    mov byte[digit1], ah
    mov byte[digit2], al
    mov eax, 4
    mov ebx, 1
    mov ecx, digit2
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, digit1
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, separator
    mov edx, 1
    int 80h
    popa
    ret
read_digits:
    ;Reading first digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h
    ;Reading second digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 2 
    int 80h
    ; convert to integer
    sub byte[digit1], 30h
    sub byte[digit2], 30h
    ret
section .bss
    digit1: resb 1
    digit2: resb 2
    num: resb 1
    factor: resb 1
section .data
    input: db 'Enter a two digit number: '
    len_input: equ $-input
    separator: db ','
    newline: db 10
