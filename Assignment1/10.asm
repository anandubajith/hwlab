;
; Input the sides of a rectangle and
; find the area and perimeter of it.
;

section .text
global _start:
_start:
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[a], al 
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[b], al 
   
    ; calculate 2 ( a + b )
    movzx ax, byte[a]
    movzx bx, byte[b]
    add ax, bx
    mov bx, 2
    mul bx
    mov word[res], ax
    call print_num

    ; calculate a * b
    movzx ax, byte[a]
    movzx bx, byte[b]
    mul bx
    mov word[res], ax
    call print_num

    jmp exit
exit:
    mov eax, 1
    mov ebx, 0
    int 80h
read_digits:
    pusha
    mov byte[digit1], 0
    mov byte[digit2], 0
    ;Reading first digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h
    ;Reading second digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 2 ;Here we put 2 because we need to read and
    int 80h    ;omit enter key press as well
    sub byte[digit1], 30h
    sub byte[digit2], 30h
    popa
    ret
print_num:
    mov byte[count],0
    pusha
extract_no:
    cmp word[res], 0
    je print_no
    inc byte[count]
    mov dx, 0
    mov ax, word[res]
    mov bx, 10
    div bx
    push dx
    mov word[res], ax
    jmp extract_no
print_no:
    cmp byte[count], 0
    je end_print
    dec byte[count]
    pop dx
    mov byte[temp], dl
    add byte[temp], 30h
    mov eax, 4
    mov ebx, 1
    mov ecx, temp
    mov edx, 1
    int 80h
    jmp print_no
end_print:
    mov eax,4
    mov ebx,1
    mov ecx,newline
    mov edx,1
    int 80h
    popa
    ret
section .bss
    digit1: resb 1
    digit2: resb 2
    a: resb 1
    b: resb 1
    res: resw 1
    area: resw 1
    peri: resw 1
    temp: resb 1
    count: resb 1
section .data
    nl: db 0Ah
    newline: db 10
