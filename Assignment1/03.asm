;
; To implement a calculator that performs the following
; operations as illustrated below
; i. Addition (Example: Input 86,13 Output: 8613)
; ii. Subtraction (Example: Input 86,13 Output: 6831)
; iii. Multiplication (Example: Input 86,13 Output: 1386)
; iv. Division (Example: Input 86,13 Output: 7380[Hint: 1380+6000])

section .text
global _start:
_start:
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[num1], al
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[num2], al 
    
    mov eax, 3
    mov ebx, 0
    mov ecx, operation
    mov edx, 2
    int 80h
    
    cmp byte[operation], 0x2b
    je addition
    cmp byte[operation], 0x2d
    je subtraction
    cmp byte[operation], 0x2a
    je multiplication
    cmp byte[operation], 0x2f
    je division

addition:
    mov word[res], 0
    mov al, byte[num1]
    mov bl, 100
    mul bl
    mov word[res], ax
    movzx cx,  byte[num2] 
    add word[res], cx
    call print_res
    jmp exit

subtraction:
    mov word[res], 0
    movzx ax, byte[num1]
    mov bl, 10
    div bl
    mov byte[tmp2], ah
    mov byte[tmp1], al
    movzx ax, byte[tmp2]
    mov bx, 1000
    mul bx
    add word[res], ax

    movzx ax, byte[tmp1]
    mov bx, 100
    mul bx
    add word[res], ax

    movzx ax, byte[num2]
    mov bl, 10
    div bl
    mov byte[tmp2], ah
    mov byte[tmp1], al 
    movzx ax, byte[tmp2]
    mov bx, 10
    mul bx
    add word[res], ax
    movzx ax, byte[tmp1]
    add word[res], ax
    movzx eax, word[res]
    call print_res
    jmp exit

multiplication:
    mov word[res], 0
    mov al, byte[num2]
    mov bl, 100
    mul bl
    mov word[res], ax
    movzx cx,  byte[num1] 
    add word[res], cx
    call print_res
    jmp exit

division:
    mov word[res], 0
    xor eax, eax
    mov al, byte[num2]
    mov bl, 100
    mul bl
    add word[res], ax
    movzx ax, byte[num1]
    mov bl, 10
    div bl
    mov byte[tmp1], ah
    mov byte[tmp2], al

    movzx ax, byte[tmp2]
    mov bx, 10
    mul bx
    add word[res], ax

    movzx ax, byte[tmp1]
    mov bx, 1000
    mul bx
    add word[res], ax
    call print_res
    jmp exit
exit:
    mov eax, 1
    mov ebx, 0
    int 80h
read_digits:
    pusha
    ;Reading first digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h
    ;Reading second digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 2 
    int 80h
    ; convert to integer
    sub byte[digit1], 30h
    sub byte[digit2], 30h
    popa
    ret
print_res:
    mov byte[count],0
    pusha
extract_no:
    cmp word[res], 0
    je print_no
    inc byte[count]
    mov dx, 0
    mov ax, word[res]
    mov bx, 10
    div bx
    push dx
    mov word[res], ax
    jmp extract_no
print_no:
    cmp byte[count], 0
    je end_print
    dec byte[count]
    pop dx
    mov byte[temp], dl
    add byte[temp], 30h
    mov eax, 4
    mov ebx, 1
    mov ecx, temp
    mov edx, 1
    int 80h
    jmp print_no
end_print:
    mov eax,4
    mov ebx,1
    mov ecx,newline
    mov edx,1
    int 80h
    popa
    ret
section .bss
    num1: resb 1
    num2: resb 1
    res: resw 1
    tmp1: resb 1
    tmp2: resb 1
    digit1: resb 1
    digit2: resb 2
    count: resb 1
    temp: resb 1
    operation: resb 2

section .data
    newline: db 0ah
  