;
; To find the greatest among 3 two digit numbers 
;
section .text
global _start:
_start:
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[a], al 
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl ;
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[b], al 
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[c], al

    mov al, byte[a];
    mov bl, byte[b]
    mov cl, byte[c]

C1:
    cmp  al, bl
    jl   C2
    cmp  al, cl
    jl   C2
    jmp  is_a

C2:
    cmp bl, cl
    jg  is_b
    jmp is_c

is_a:
    mov byte[res], al
    jmp print
is_b:
    mov byte[res], bl
    jmp print
is_c:
    mov byte[res], cl
    jmp print
exit:
    mov eax, 1
    mov ebx, 0
    int 80h
read_digits:
    pusha
    mov byte[digit1], 0
    mov byte[digit2], 0
    ;Reading first digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h
    ;Reading second digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 1 ;Here we put 2 because we need to read and
    int 80h    ;omit enter key press as well
    sub byte[digit1], 30h
    sub byte[digit2], 30h
    mov eax, 3
    mov ebx, 0
    mov ecx, buf
    mov edx, 1
    int 80h
    popa
    ret
print: 
    ; to print a number
    ; divide by 10 , print divisor and remainder
    ; 81 / 10 => 8 and 1
    ; res has the number
    movzx ax, byte[res]
    mov bl, 10
    div bl ; ah-> remainder al -> quotient
    mov byte[c1], al
    mov byte[c2], ah
    add byte[c1], 30h
    add byte[c2], 30h

    mov eax, 4
    mov ebx, 1
    mov ecx, c1
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, c2
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    jmp exit
section .bss
    digit1: resb 1
    digit2: resb 1
    a: resb 1
    b: resb 1
    c: resb 1
    res: resb 1
    buf: resb 1
    c1: resb 1
    c2: resb 1
section .data
    nl: db 0Ah
