;
; check if given two digit number is 
; prime or not
;
section .text
global _start:
_start:
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[num], al
    movzx ax, byte[num]
    mov bl, 2
    div bl
    mov dl, al ; dl, has num/2
    mov cl, 2
loop:
    movzx ax, byte[num]
    div cl
    cmp ah, 0 ; checking if remainder is zero
    je not_prime
    inc cl
    cmp cl, dl
    jb loop
    jmp prime
exit:
    mov eax, 1
    mov ebx, 0
    int 80h
prime: 
    mov eax, 4
    mov ebx, 0
    mov ecx, is_prime
    mov edx, len_is_prime
    int 80h
    jmp exit
not_prime: 
    mov eax, 4
    mov ebx, 0
    mov ecx, is_not_prime
    mov edx, len_is_not_prime
    int 80h
    jmp exit
read_digits:
    ;Reading first digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h
    ;Reading second digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 2 
    int 80h
    ; convert to integer
    sub byte[digit1], 30h
    sub byte[digit2], 30h
    ret
section .bss
    digit1: resb 1
    digit2: resb 2
    num: resb 1
section .data
    input: db 'Enter a three digit number: '
    len_input: equ $-input
    is_prime: db 'Given input is prime',0Ah
    len_is_prime: equ $-is_prime
    is_not_prime: db 'Given input is not prime',0Ah
    len_is_not_prime: equ $-is_not_prime
