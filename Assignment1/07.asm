;
; To find LCM of 2 two digit numbers
;

section .text
global _start:
_start:
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[a], al

    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[b], al

    mov dh, byte[a]
    mov dl, byte[b]

    cmp dh, dl
    jg a_b

calc_lcm:
    mov dl, byte[a]
lcm_loop:   
    xor ah, ah
    mov al, dl
    mov bl, byte[b]
    div bl
    cmp ah, 0
    mov byte[lcm], dl
    je print_num
    mov cl, byte[a]
    add dl, cl
    jmp lcm_loop

exit:
    mov eax, 1
    mov ebx, 0
    int 80h
read_digits:
    ;Reading first digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h
    ;Reading second digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 2 
    int 80h
    ; convert to integer
    sub byte[digit1], 30h
    sub byte[digit2], 30h
    ret
a_b:
    mov byte[a], dh
    mov byte[b], dl
    call calc_lcm

print_num:
    mov byte[count],0
    pusha
extract_no:
    cmp word[lcm], 0
    je print_no
    inc byte[count]
    mov dx, 0
    mov ax, word[lcm]
    mov bx, 10
    div bx
    push dx
    mov word[lcm], ax
    jmp extract_no
print_no:
    cmp byte[count], 0
    je end_print
    dec byte[count]
    pop dx
    mov byte[temp], dl
    add byte[temp], 30h
    mov eax, 4
    mov ebx, 1
    mov ecx, temp
    mov edx, 1
    int 80h
    jmp print_no
end_print:
    mov eax,4
    mov ebx,1
    mov ecx,newline
    mov edx,1
    int 80h
    popa
    jmp exit
section .bss
    digit1: resb 1
    digit2: resb 2
    lcm: resw 1
    a: resb 1
    b: resb 1
    count: resb 1
    temp: resb 1
section .data
    nl: db 0Ah
    newline: db 10

