;
; To read two distinct numbers and display
; the even numbers between the numbers.
;

section .text
global _start:
_start:
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[num1], al 
    call read_digits
    mov al, byte[digit1]
    mov bl, 10
    mul bl
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[num2], al 

    mov al, byte[num1]
    and al, 1
    jnz make_even
    jmp print_loop

exit:
    mov eax, 1
    mov ebx, 0
    int 80h

make_even:
    ; incase the starting num1
    ; is not even, we add 1
    inc byte[num1]
    jmp print_loop

print_loop:
    ; this funciton splits the number
    ; and prints it into stdout
    ; repeats until num1<=num2
    movzx ax, byte[num1]
    mov bl, 10
    div bl
    add ah, 30h
    add al, 30h
    mov byte[digit1], ah
    mov byte[digit2], al
    mov eax, 4
    mov ebx, 1
    mov ecx, digit2
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, digit1
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h

    add byte[num1], 2
    movzx eax, byte[num1]
    movzx ebx, byte[num2]
    cmp eax, ebx
    jl print_loop
    jmp exit
    ; compare and jump

read_digits:
    pusha
    ;Reading first digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h
    ;Reading second digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 2 
    int 80h
    sub byte[digit1], 30h
    sub byte[digit2], 30h
    popa
    ret
section .bss
    digit1: resb 1
    digit2: resb 2
    num1: resb 1
    num2: resb 1
section .data
    nl: db 0Ah ; just a newline constant