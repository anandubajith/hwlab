;
; Input three two-digit numbers and if one is equal to
; the sum of the other two numbers print the number
;
section .text
global _start:
_start:
    ; read first 2 digit number
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[num1], al 
    ; read second 2 digit number
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2]
    add ax, bx
    mov byte[num2], al 
    ; read third 2 digit number
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl
    movzx bx, byte[digit2]
    add ax, bx
    mov byte[num3], al 
    ; move the numbers to 
    ; registers for operations
    mov al, byte[num1]
    mov bl, byte[num2]
    mov cl, byte[num3]

    ; precalculate a+b, b+c, a+c
    mov byte[a_b], al
    add byte[a_b], bl

    mov byte[b_c], bl
    add byte[b_c], cl

    mov byte[c_a], cl
    add byte[c_a], al

    ; jump based on the case
    ; if none matches jump to none case
    cmp byte[c_a], bl
    je case_b

    cmp byte[a_b], cl
    je case_c

    cmp byte[b_c], al
    je case_a
    jmp none
exit:
    mov eax, 1
    mov ebx, 0
    int 80h
print:
    ; print a two digit number
    movzx ax, byte[res]
    mov bl, 10
    div bl
    add ah, 30h
    add al, 30h
    mov byte[digit1], ah
    mov byte[digit2], al
    mov eax, 4
    mov ebx, 1
    mov ecx, digit2
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, digit1
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, 10
    mov edx, 1
    int 80h
    jmp exit
case_a:
    mov byte[res], al
    call print
case_b:
    mov byte[res], bl
    call print
case_c:
    mov byte[res],cl
    call print
read_digits:
    pusha
    ;Reading first digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h
    ;Reading second digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 2 
    int 80h
    ; convert to integer
    sub byte[digit1], 30h
    sub byte[digit2], 30h
    popa
    ret

none:
    ; when none of
    ; the digits match
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_none
    mov edx, len_msg_none
    int 80h
    jmp exit

section .bss
    digit1: resb 1
    digit2: resb 2
    num1: resb 1
    num2: resb 1
    num3: resb 1
    res: resb 1
    a_b: resb 1
    b_c: resb 1
    c_a: resb 1

section .data
    msg_none: db 'None', 0Ah
    len_msg_none: equ $-msg_none