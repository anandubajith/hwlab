;
; check if given three digit number is 
; palidrome or not
;
section .text
global _start:
_start:
    mov eax, 4
    mov ebx, 0
    mov ecx, input
    mov edx, len_input
    int 80h
    mov eax, 3
    mov ebx, 1
    mov ecx, c1
    mov edx, 1
    int 80h
    mov eax, 3
    mov ebx, 1
    mov ecx, c2
    mov edx, 1
    int 80h
    mov eax, 3
    mov ebx, 1
    mov ecx, c3
    mov edx, 1
    int 80h
    mov al, byte[c1]
    mov ah, byte[c3]
    cmp al,ah
    je palidrome
    jne not_palidrome
exit:
    mov eax, 1
    mov ebx, 0
    int 80h
palidrome: 
    mov eax, 4
    mov ebx, 0
    mov ecx, is_palidrome
    mov edx, len_is_palidrome
    int 80h
    jmp exit
not_palidrome: 
    mov eax, 4
    mov ebx, 0
    mov ecx, is_not_palidrome
    mov edx, len_is_not_palidrome
    int 80h
    jmp exit

section .bss
    c1: resb 1
    c2: resb 1
    c3: resb 1
section .data
    input: db 'Enter a three digit number: '
    len_input: equ $-input
    is_palidrome: db 'Given input is palidrome',0Ah
    len_is_palidrome: equ $-is_palidrome
    is_not_palidrome: db 'Given input is not palidrome',0Ah
    len_is_not_palidrome: equ $-is_not_palidrome