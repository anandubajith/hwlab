;
; Write a program to read 2 strings and compare them.
;
section .text
global _start
_start:

	mov dword[loc], s1
	call read_string
	mov cl, byte[buf_len]
	mov byte[len_s1], cl
	mov dword[loc], s2
	call read_string
	mov cl, byte[buf_len]
	mov byte[len_s2], cl


	mov cl, byte[len_s1]
	cmp cl, byte[len_s2]
	jne not
	; compare s1, and s2
	mov eax, s1
	mov ebx, s2
	mov cl, byte[len_s1]
	loop:
		mov dl, byte[eax]
		cmp dl, byte[ebx]
		jne not
		inc eax
		inc ebx
		dec cl
		cmp cl, 0
		jg loop
		
	
yes:
	mov eax, 4
	mov ebx, 1
	mov ecx, msg_yes
	mov edx, len_msg_yes
	int 80h
	jmp exit	
not: 
	mov eax, 4
	mov ebx, 1
	mov ecx, msg_no
	mov edx, len_msg_no
	int 80h
	jmp exit
exit:
	mov eax, 1
	mov ebx, 0
	int 80h
read_string:
	pusha
	mov ebx, dword[loc] 
	mov byte[buf_len], 0
read_char:
	pusha
	mov eax, 3
	mov ebx, 0
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	cmp byte[tmp], 10
	je end_read_string
	inc byte[buf_len]
	mov al, byte[tmp]
	mov byte[ebx], al
	inc ebx
	jmp read_char
end_read_string:
	mov byte[ebx], 0
	popa
	ret
print_string:
	pusha
	mov ebx, dword[loc]
print_char:
	mov al, byte[ebx]
	mov byte[tmp], al
	cmp al, 0
	je end_print_string
	pusha
	mov eax, 4
	mov ebx, 1
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	inc ebx
	jmp print_char
end_print_string:
	popa
	ret

section .bss
	buf: resb 100
	s1: resb 100
	s2: resb 100
	len_s1: resb 1
	len_s2: resb 1
	buf_len: resb 1
	tmp: resb 1
	loc: resd 1

section .data
	msg_yes: db 'The strings are same', 10
	len_msg_yes: equ $-msg_yes
	msg_no: db 'The strings are not the same', 10
	len_msg_no: equ $-msg_no
