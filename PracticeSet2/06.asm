;
; Write a program to count the number of vowels in a given string
;
section .text
global _start
_start:

	call read_string
	mov eax, buf
	movzx ebx, byte[buf_len]
	mov byte[count], 0
	loop:
		cmp byte[eax], 'a'
		je increment
		cmp byte[eax], 'A'
		je increment
		cmp byte[eax], 'e'
		je increment
		cmp byte[eax], 'E'
		je increment
		cmp byte[eax], 'i'
		je increment
		cmp byte[eax], 'I'
		je increment
		cmp byte[eax], 'o'
		je increment
		cmp byte[eax], 'O'
		je increment
		cmp byte[eax], 'u'
		je increment
		cmp byte[eax], 'U'
		je increment
		jmp c	
		increment:
			inc byte[count]
		c:
			inc eax
			dec ebx
			cmp ebx, 0
		jg loop

		;; print count
		movzx bx, byte[count]
		mov word[buf], bx
		call print
exit:
	mov eax, 1
	mov ebx, 0
	int 80h
read_string:
	pusha
	mov ebx, buf
	mov byte[buf_len], 0
read_char:
	pusha
	mov eax, 3
	mov ebx, 0
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	cmp byte[tmp], 10
	je end_read_string
	inc byte[buf_len]
	mov al, byte[tmp]
	mov byte[ebx], al
	inc ebx
	jmp read_char
end_read_string:
	mov byte[ebx], 0
	popa
	ret
print_string:
	pusha
	mov ebx, buf
print_char:
	mov al, byte[ebx]
	mov byte[tmp], al
	cmp al, 0
	je end_print_string
	pusha
	mov eax, 4
	mov ebx, 1
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	inc ebx
	jmp print_char
end_print_string:
	popa
	ret
print:
    mov byte[count], 0
    pusha
    cmp word[buf], 0
    je p0
extract_digits:
    cmp word[buf], 0
    je print_digits
    inc byte[count]
    mov dx, 0
    mov ax, word[buf]
    mov bx, 10
    div bx
    push dx
    mov word[buf], ax
    jmp extract_digits
print_digits:
    cmp byte[count], 0
    je end_print
    dec byte[count]
    pop dx
    mov byte[tmp], dl
    add byte[tmp], 30h
    mov eax, 4
    mov ebx, 1
    mov ecx, tmp
    mov edx, 1
    int 80h
    jmp print_digits
end_print:
    mov eax,4
    mov ebx,1
    mov ecx, separator
    mov edx, 1
    int 80h
    popa
    ret
p0:
    mov eax, 4
    mov ebx, 1
    mov ecx, zero
    mov edx, 1
    int 80h
    jmp end_print

section .bss
	buf: resb 100
	buf_len: resb 1
	tmp: resb 1
	count: resb 1
section .data
	zero: db '0'
	separator: db 10
