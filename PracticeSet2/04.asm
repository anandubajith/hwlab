;
; Write a program to remove duplicates from a string.
; (Example :bananas:bans)
;
section .text
global _start
_start:

	mov dword[loc],buf 
	call read_string

	mov eax, buf
	movzx ebx, byte[buf_len]
	mov ecx, res
	loop:
		sub byte[eax], 'a'
		movzx edx, byte[eax]
		cmp byte[seen+edx], 0
		jne skip
			mov byte[seen+edx], 1
			add byte[eax],'a'
			mov dl, byte[eax]
			mov byte[ecx], dl
			inc ecx
		skip:
		inc eax
		dec ebx
		cmp ebx, 0
		jg loop
		
	call print_string
exit:
	mov eax, 1
	mov ebx, 0
	int 80h
read_string:
	pusha
	mov ebx, dword[loc] 
	mov byte[buf_len], 0
read_char:
	pusha
	mov eax, 3
	mov ebx, 0
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	cmp byte[tmp], 10
	je end_read_string
	inc byte[buf_len]
	mov al, byte[tmp]
	mov byte[ebx], al
	inc ebx
	jmp read_char
end_read_string:
	mov byte[ebx], 0
	popa
	ret
print_string:
	pusha
	mov ebx, dword[loc]
print_char:
	mov al, byte[ebx]
	mov byte[tmp], al
	cmp al, 0
	je end_print_string
	pusha
	mov eax, 4
	mov ebx, 1
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	inc ebx
	jmp print_char
end_print_string:
	popa
	ret

section .bss
	buf: resb 100
	buf_len: resb 1
	result: resb 100
	tmp: resb 1
	loc: resd 1
	res: resb 100
section .data
	seen: times 100 db 0
