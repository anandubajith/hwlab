;
; Write a program to check whether a string is a palindrome or not
;
section .text
global _start
_start:

	call read_string
	
	mov eax, buf
	mov ebx, buf 
	movzx ecx, byte[buf_len]
	dec ecx
	add ebx, ecx
	loop:
		mov cl, byte[eax]
		cmp cl, byte[ebx]
		jne not_palindrome 
		inc eax
		dec ebx
		cmp eax, ebx
		jle loop
palidrome:
	mov eax, 4
	mov ebx, 1
	mov ecx, is_palidrome
	mov edx, len_is_palidrome
	int 80h
	jmp exit
not_palindrome:
	mov eax, 4
	mov ebx, 1
	mov ecx, is_not_palidrome
	mov edx, len_is_not_palidrome
	int 80h
	jmp exit
exit:
	mov eax, 1
	mov ebx, 0
	int 80h

read_string:
	pusha
	mov ebx, buf
	mov byte[buf_len], 0
read_char:
	pusha
	mov eax, 3
	mov ebx, 0
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	cmp byte[tmp], 10
	je end_read_string
	inc byte[buf_len]
	mov al, byte[tmp]
	mov byte[ebx], al
	inc ebx
	jmp read_char
end_read_string:
	mov byte[ebx], 0
	popa
	ret
print_string:
	pusha
	mov ebx, buf
print_char:
	mov al, byte[ebx]
	mov byte[tmp], al
	cmp al, 0
	je end_print_string
	pusha
	mov eax, 4
	mov ebx, 1
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	inc ebx
	jmp print_char
end_print_string:
	popa
	ret

section .bss
	buf: resb 100
	buf_len: resb 1
	tmp: resb 1
section .data
	is_palidrome: db 'The given string is palidrome', 10
	len_is_palidrome: equ $-is_palidrome
	is_not_palidrome: db 'The given string is not palidrome', 10
	len_is_not_palidrome: equ $-is_not_palidrome
