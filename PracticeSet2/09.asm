;
; Write a program to accept astring and encrypt the string in such
; a way that each character is replaced by the immediate next character.
; Example: brain as csbjo
;
%define SHIFT_AMOUNT 1
section .text
global _start
_start:

	call read_string
	mov eax, buf
	movzx ebx, byte[buf_len]
	loop:
		mov cl, byte[eax]
		mov byte[tmp], cl
		call encode
		mov cl, byte[tmp]
		mov byte[eax],cl
		inc eax
		dec ebx
		cmp ebx, 0
		jg loop

	call print_string
exit:
	mov eax, 1
	mov ebx, 0
	int 80h

encode:
	pusha
	movzx ax, byte[tmp]
	cmp ax, 'a'
	jl else
	cmp ax, 'z'
	jg else
		sub ax, 'a'
		add ax, SHIFT_AMOUNT
		mov bl, 26
		div bl
		add ah, 'a'
		mov byte[tmp], ah
		jmp c
	else:
		cmp ax, 'A'
		jl c
		cmp ax, 'Z'
		jg c
		sub ax, 'A'
		add ax, SHIFT_AMOUNT
		mov bl, 26
		div bl
		add ah, 'A'
		mov byte[tmp], ah
	c:
	popa
	ret
read_string:
	pusha
	mov ebx, buf
	mov byte[buf_len], 0
read_char:
	pusha
	mov eax, 3
	mov ebx, 0
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	cmp byte[tmp], 10
	je end_read_string
	inc byte[buf_len]
	mov al, byte[tmp]
	mov byte[ebx], al
	inc ebx
	jmp read_char
end_read_string:
	mov byte[ebx], 0
	popa
	ret
print_string:
	pusha
	mov ebx, buf
print_char:
	mov al, byte[ebx]
	mov byte[tmp], al
	cmp al, 0
	je end_print_string
	pusha
	mov eax, 4
	mov ebx, 1
	mov ecx, tmp
	mov edx, 1
	int 80h
	popa
	inc ebx
	jmp print_char
end_print_string:
	popa
	ret

section .bss
	buf: resb 100
	buf_len: resb 1
	tmp: resb 1
