;
; Read an array of n two digit numbers. Read another number k, 
; and perform linear search to findif k is present in the array or not.
; If present print 1 else print 0. 
;

%define SIZE 100
section .text
global _start:
_start:
    ; read n
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[n], al

    ; read the loop numbers
    mov eax, 0
    movzx ecx, byte[n]
loop:
    ; read num
    call read_tmp
    mov dl,byte[temp]
    mov byte[ar+eax], dl
    inc eax
    cmp eax, ecx
    jb loop
    
    ; get the number to search
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[k], al

    mov eax, 0
    movzx ecx, byte[n]
search:
    ; read num
    mov dl, byte[ar+eax]
    cmp dl, byte[k]
    je found
    inc eax
    cmp eax, ecx
    jb search
    jmp not_found

exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    mov eax, 1
    mov ebx, 0
    int 80h
found: 
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_found
    mov edx, 1
    int 80h
    jmp exit
not_found:
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_not_found
    mov edx, 1
    int 80h
    jmp exit
read_tmp:
    pusha
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[temp], al
    popa
    ret
read_digits:
    pusha
    ;Reading first digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h
    ;Reading second digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 2 
    int 80h
    ; convert to integer
    sub byte[digit1], 30h
    sub byte[digit2], 30h
    popa
    ret
section .bss
    digit1: resb 1
    digit2: resb 2
    n: resb 1
    temp:resb 1
    k: resb 1
    ar: times SIZE resb 1
section .data
    nl: db 10
    newline: db 10
    msg_found: db '1'
    msg_not_found: db '0'