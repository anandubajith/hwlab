;
; Read an array of n two digit numbers. Print the largest 
; and smallest element in the array
;
%define SIZE 100
section .text
global _start:
_start:
    ; read n
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[n], al

    ; read the loop numbers
    mov eax, 0
    movzx ecx, byte[n]
loop:
    ; read num
    call read_tmp
    mov dl,byte[temp]
    mov byte[ar+eax], dl
    inc eax
    cmp eax, ecx
    jb loop

    mov eax, 0
    movzx ecx, byte[n]

    ; loop through the numbers ,
    ; if less than curMin , set curMin
    ; if greater than curMax, set curMax

    mov byte[min], 100
    mov byte[max], 0
    ; read the loop numbers
    mov eax, 0
    movzx ecx, byte[n]

loop2:
    mov dl, byte[ar+eax]
    cmp dl, byte[min]
    ja c2
    mov byte[min], dl
c2:
    cmp dl, byte[max]
    jb c3
    mov byte[max], dl
c3:
    inc eax
    cmp eax, ecx
    jb loop2

    mov dl, byte[min]
    mov byte[temp], dl
    call print
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    mov dl, byte[max]
    mov byte[temp], dl
    call print

exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    mov eax, 1
    mov ebx, 0
    int 80h
read_tmp:
    pusha
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[temp], al
    popa
    ret
read_digits:
    pusha
    ;Reading first digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h
    ;Reading second digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 2 
    int 80h
    ; convert to integer
    sub byte[digit1], 30h
    sub byte[digit2], 30h
    popa
    ret
print:
    pusha
    ; print a two digit number
    movzx ax, byte[temp]
    mov bl, 10
    div bl
    add ah, 30h
    add al, 30h
    mov byte[digit1], ah
    mov byte[digit2], al
    mov eax, 4
    mov ebx, 1
    mov ecx, digit2
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, digit1
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, 10
    mov edx, 1
    int 80h
    popa
    ret

section .bss
    digit1: resb 1
    digit2: resb 2
    n: resb 1
    temp:resb 1
    ar: times SIZE resb 1
    min: resb 1
    max: resb 1
section .data
    nl: db 10
    newline: db 10