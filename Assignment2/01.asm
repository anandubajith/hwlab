;
; Read n two digit numbers to an array and find their average. 
; Print the count of numbers that are above the average
;

%define SIZE 100
section .text
global _start
_start:
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[n], al

    mov eax, 0
    mov byte[sum], 0
    movzx ecx, byte[n]
loop:
    ; read num
    call read_tmp
    mov dl,byte[temp]
    mov byte[ar+eax], dl
    mov dh, 0
    add word[sum], dx
    inc eax
    cmp eax, ecx
    jb loop
    
    ; calculate average
    mov ax, word[sum]
    mov bl, byte[n]
    div bl

    ; al has the quotient;
    mov byte[avg], al
    
    movzx ecx, byte[n]
    mov eax, 0
    mov byte[count], 0

loop2:
    mov dh, byte[ar+eax]
    cmp dh, byte[avg]
    jna continue
    inc byte[count]
continue:
    inc eax
    cmp eax, ecx
    jb loop2

    call print
    jmp exit

exit:
    mov eax, 4
    mov ebx, 1
    mov ecx, nl
    mov edx, 1
    int 80h
    mov eax, 1
    mov ebx, 0
    int 80h
read_tmp:
    pusha
    call read_digits
    mov al, byte[digit1] 
    mov bl, 10
    mul bl 
    movzx bx, byte[digit2] 
    add ax, bx
    mov byte[temp], al
    popa
    ret
read_digits:
    pusha
    ;Reading first digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h
    ;Reading second digit
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 2
    int 80h
    ; convert to integer
    sub byte[digit1], 30h
    sub byte[digit2], 30h
    popa
    ret
print:
    ; print a two digit number
    movzx ax, byte[count]
    mov bl, 10
    div bl
    add ah, 30h
    add al, 30h
    mov byte[digit1], ah
    mov byte[digit2], al
    mov eax, 4
    mov ebx, 1
    mov ecx, digit2
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, digit1
    mov edx, 1
    int 80h
    mov eax, 4
    mov ebx, 1
    mov ecx, 10
    mov edx, 1
    int 80h
    jmp exit
section .bss
    digit1: resb 1
    digit2: resb 2
    n: resb 1
    temp:resb 1
    ar: times SIZE resb 1 
    sum: resw 1
    count: resb 1
    avg: resb 1
section .data
    nl: db 10
    newline: db 10