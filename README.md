# Hardware Laboratory

## Instructions

- Install nasm
- Set the question number in `Makefile`
- Compile and link the program by executing `make`
- Execute the program by `./<QuestionNumber>.out`

```bash
    cd Assignment1
    make
    ./01.out
```

## NeoVim Binding for faster execution
```
autocmd filetype asm nnoremap <C-r> :w <bar> te make Q_NO=%:r && ./%:r.out<CR> <ESC> :startinsert <CR>
```
