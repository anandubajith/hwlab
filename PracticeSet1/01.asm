;
; Print name, address to stdout
;
section .text
global _start:
_start:
    ; print name
    mov eax, 4
    mov ebx, 1
    mov ecx, name
    mov edx, len_name
    int 80h
    ; print address1
    mov eax, 4
    mov ebx, 1
    mov ecx, address1
    mov edx, len_address1
    int 80h
    ; print address2
    mov eax, 4
    mov ebx, 1
    mov ecx, address2
    mov edx, len_address2
    int 80h
    ; exit 
    mov eax, 1
    mov ebx, 0
    int 80h
section .data
    name: db 'Anandu B Ajith', 0Ah
    len_name: equ $-name
    address1: db 'Ganga, Vengeri', 0Ah
    len_address1: equ $-address1
    address2: db 'Kozhikode', 0Ah
    len_address2: equ $-address2