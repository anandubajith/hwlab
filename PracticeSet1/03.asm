;
; Find the Endianness of the system
;
section .text
global _start
_start:
    cmp byte[w], 041h
    je yes
    jmp no
exit: 
    mov eax, 1
    mov ebx, 0
    int 80h
yes:
    mov eax, 4
    mov ebx, 1
    mov ecx, y
    mov edx, len_y
    int 80h
    jmp exit
no: 
    mov eax, 4
    mov ebx, 1
    mov ecx, n 
    mov edx, len_n
    int 80h
    jmp exit
section .data
    w: dw 'AB'
    y: db 'LittleEndian',0Ah
    len_y: equ $-y
    n: db 'BigEndian', 0Ah
    len_n: equ $-n