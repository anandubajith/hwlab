;
; Read a character and check if CapsLock is on
;
section .text
global _start
_start:
    ; display message
    mov eax, 4
    mov ebx, 1
    mov ecx, msg
    mov edx, len_msg
    int 80h
    ; reading a character
    mov eax, 3
    mov ebx, 0
    mov ecx, char
    mov edx, 1
    int 80h

    cmp byte[char], byte 'A'
    jl insufficent_info
    cmp byte[char], byte 'z'
    jg insufficent_info

    ; check if between 'A' and 'z'
    ; if less than 'Z' => CAPS_ON
    ; if greater than 'a' => CAPS_OFF
    ; else insufficent
    cmp byte[char], byte 'Z'
    jna caps_on
    cmp byte[char], byte 'a'
    jnb caps_off
    
    jmp insufficent_info

exit:
    mov eax, 1
    mov ebx, 0
    int 80h
insufficent_info:
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_insufficent_info
    mov edx, len_msg_insufficent_info
    int 80h
    jmp exit
caps_on:
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_caps_on
    mov edx, len_msg_caps_on
    int 80h
    jmp exit
caps_off:
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_caps_off
    mov edx, len_msg_caps_off
    int 80h
    jmp exit

section .bss
    char: resb 1
section .data
    msg: db 'Enter a character: '
    len_msg: equ $-msg

    msg_caps_on: db 'Caps Lock is ON',0Ah
    len_msg_caps_on: equ $-msg_caps_on

    msg_caps_off: db 'Caps Lock is OFF', 0Ah
    len_msg_caps_off: equ $-msg_caps_off

    msg_insufficent_info: db 'Insufficent info',0Ah
    len_msg_insufficent_info: equ $-msg_insufficent_info
