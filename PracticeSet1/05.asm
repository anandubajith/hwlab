;
; Read single digit number and find odd or even
;
section .text
global _start
_start:
    ; print message
    mov eax, 4
    mov ebx, 1
    mov ecx, msg
    mov edx, len_msg
    int 80h
    ; read a character
    mov eax, 3
    mov ebx, 0
    mov ecx, input
    mov edx, 1
    int 80h
    ; check if input is within range
    cmp byte[input],030h
    jl invalid_input
    cmp byte[input], 039h
    jg invalid_input
    ; convert to actual number
    sub byte[input], 30h
    ; divide by 2 and check remainder
    movzx eax, byte[input]
    mov edx, 0
    mov ebx, 2
    div ebx
    cmp edx, 0
    jne odd_case

even_case:
    mov eax, 4
    mov ebx, 1
    mov ecx, even
    mov edx, len_even
    int 80h
    jmp exit
odd_case:
    mov eax, 4
    mov ebx, 1
    mov ecx, odd
    mov edx, len_odd
    int 80h
    jmp exit
exit:
    mov eax, 1
    mov ebx, 0
    int 80h
invalid_input:
    mov eax, 4
    mov ebx, 1
    mov ecx, invalid
    mov edx, len_invalid
    int 80h
    jmp exit

section .bss
    input: resb 1

section .data

    msg: db 'Enter a number: '
    len_msg: equ $-msg

    odd: db 'ODD', 0Ah
    len_odd: equ $-odd

    even: db 'EVEN', 0Ah
    len_even: equ $-even

    invalid: db 'INVALID INPUT', 0Ah
    len_invalid: equ $-invalid