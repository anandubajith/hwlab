section .text
global _start
_start:
    mov byte[i], 0

loop:
    inc byte[i]
    mov byte[j], 0
    jmp loop2
    cmp byte[i], 5
    jl loop
    jmp exit

loop2:
    inc byte[j]
    mov eax, 4
    mov ebx, 1
    mov ecx, symbol
    mov edx, 1
    int 80h
    movzx eax, byte[i] 
    movzx ebx, byte[j]
    cmp eax, ebx
    jl loop2
    j loop


exit:
    mov eax, 1
    mov ebx, 0
    int 80h

section .bss
    i: resb 1
    j: resb 1
    
section .data
    symbol: db '*'
    s2: db 0Ah