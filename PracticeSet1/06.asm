; read a character to c
; check if its valid
; move it to n
; read second character to c
; check if its not '\n'
;   n * 10 + ( c - '0')
; mov n to eax , and mul with n

section .text
global _start
_start:
    mov eax,3
    mov ebx, 0
    mov ecx, digit1
    mov edx, 1
    int 80h

    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 1
    int 80h
    ; cmp byte[digit2], 0x0a
    ; je c1 encountered new line so jump
    mov eax, 3
    mov ebx, 0
    mov ecx, digit2
    mov edx, 1
    int 80h

    sub byte[digit1], 30h
    sub byte[digit2], 30h
    
    mov al, byte[digit1]

    mov bl, 10
    mul bl
    movzx bx, byte[digit2]
    add ax, bx

    mov byte[num], al

    ; print byte[num]

    mov eax, 1
    movzx ebx, byte[num]
    int 80h

section .bss
    digit1: resb 1
    digit2: resb 1
    num: resb 1
section .data
