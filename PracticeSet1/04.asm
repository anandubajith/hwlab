;
; Find the largest among two single digit numbers
;
section .text
global _start
_start:
    ; print message
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_input_a
    mov edx, len_msg_input_a
    int 80h
    ; read a character
    mov eax, 3
    mov ebx, 0
    mov ecx, a
    mov edx, 1
    int 80h
    ; check if input is within range
    cmp byte[a],030h
    jl invalid_input
    cmp byte[a], 039h
    jg invalid_input
    ; convert to actual number
    sub byte[a], 30h

    ; grab newline
    mov eax, 3
    mov ebx, 0
    mov ecx, b
    mov edx, 1
    int 80h

    ; print message
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_input_b
    mov edx, len_msg_input_b
    int 80h
    ; read a character
    mov eax, 3
    mov ebx, 0
    mov ecx, b
    mov edx, 1
    int 80h
    ; check if input is within range
    cmp byte[b],030h
    jl invalid_input
    cmp byte[b], 039h
    jg invalid_input
    ; convert to actual number
    sub byte[b], 30h

    ; move to registers and compare
    movzx eax, byte[b]
    movzx ebx, byte[a]
    cmp ebx, eax
    je a_equal_b
    jl a_less_b
    jg a_greater_b

exit:
    mov eax, 1
    mov ebx, 0
    int 80h
invalid_input:
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_invalid
    mov edx, len_msg_invalid
    int 80h
    jmp exit
a_equal_b:
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_equal
    mov edx, len_msg_equal
    int 80h
    jmp exit
a_less_b:
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_less
    mov edx, len_msg_less
    int 80h
    jmp exit
a_greater_b:
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_greater
    mov edx, len_msg_greater
    int 80h
    jmp exit
section .bss
    a: resb 1
    b: resb 1
section .data:
    msg_input_a: db 'Enter A = '
    len_msg_input_a: equ $-msg_input_a
    msg_input_b: db 'Enter B = '
    len_msg_input_b: equ $-msg_input_b
    
    msg_equal: db 'A = B',0Ah
    len_msg_equal: equ $-msg_equal

    msg_less: db 'A < B',0Ah
    len_msg_less: equ $-msg_less

    msg_greater: db 'A > B', 0Ah
    len_msg_greater: equ $-msg_greater

    msg_invalid: db 'INVALID INPUT', 0Ah
    len_msg_invalid: equ $-msg_invalid