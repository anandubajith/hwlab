section .text
global _start
_start:
    mov eax, 3
    mov ebx, 0
    mov ecx, input
    mov edx, 1
    int 80h

    sub byte[input], 30h
    movzx eax, byte[input]
    mov ebx, 0
    call loop

loop:
    inc ebx
    pusha
    mov eax, 4
    mov ebx, 1
    mov ecx, 38h
    mov edx, 1
    int 80h
    popa
    cmp eax, ebx
    jmp loop

exit:
    mov eax, 1
    mov ebx, 0
    int 80h

section .bss
    input: resb 1